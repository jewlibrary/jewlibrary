package es.uned.jewlibrary;

import javax.swing.JFrame;

/**
 * ReusableSimpleDialog
 
 Implementación del GUI de una sola página (simple)
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableSimpleDialog extends ReusableExtensibleDialog {
    /**
     * nº máximo de GUIs simples que se pueden integrar en una página
     */
    public static final int MAX_GUI_SIMPLE = 2;

    /**
     * Constructor
     * 
     * @param gui el gui que inicializa el dialogo
     * @param caption el nombre del contenedor
     */
    public ReusableSimpleDialog(ReusableDialog gui, String caption) {
        super();
        this.gui = gui;
        this.contName = caption;
    }

    /**
     * Inicializar diálogo
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario
     */
    @Override
    public Boolean setUpDialog() {
        return false;
    }
}
