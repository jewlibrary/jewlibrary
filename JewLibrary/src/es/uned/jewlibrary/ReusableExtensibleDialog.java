package es.uned.jewlibrary;

import java.util.ArrayList;
import javax.swing.JFrame;

/**
 * ReusableExtensibleDialog
 * 
 * @author Marcos Martínez Poladura
 */
public abstract class ReusableExtensibleDialog extends ReusableDialog {
    protected String contName;
        
    /**
     * Prepara la interfaz adjunta para su uso
     * 
     * @return Boolean true si se ha preparado con éxito, false si hubo algún problema
     */
    public abstract Boolean setUpDialog();
    
    /**
     * Añade un GUI hijo
     * 
     * @param extensibleChild ReusableExtensibleDialog GUI a añadir
     * @param caption String etiqueta del contenedor
     */
    public void addExtensibleChild(ReusableExtensibleDialog extensibleChild, String caption) {
        extensibleChild.setContainerName(caption);
        this.gui.getChildrenList().add(extensibleChild);
    }

    /**
     * añade lista de GUIs hijos
     * 
     * @param extensibleChildrenList ArrayList lista de GUIs hijos
     * @param caption String etiqueta del contenedor
     */
    public void addExtensibleChildrenList(ArrayList<ReusableExtensibleDialog> extensibleChildrenList, String caption) {
        for(var child: extensibleChildrenList) {
            addExtensibleChild(child, caption);
        }
    }
        
    /**
     * Establece el nombre del contenedor
     * 
     * @param contName String nombre del contenedor
     */
    public void setContainerName(String contName) {
        this.contName = contName;
    }
}
