package es.uned.jewlibrary.exceptions;

/**
 * Excepción en la operación de resetear
 *
 * @author Marcos Martínez Poladura
 */
public class ReusableCleanException extends Exception {
    
}
