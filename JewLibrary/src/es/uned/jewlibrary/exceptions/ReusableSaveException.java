package es.uned.jewlibrary.exceptions;

/**
 * Excepción en la operación de guardar
 *
 * @author Marcos Martínez Poladura
 */
public class ReusableSaveException extends Exception {
    
}
