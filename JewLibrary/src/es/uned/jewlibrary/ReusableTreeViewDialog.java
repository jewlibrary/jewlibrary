package es.uned.jewlibrary;

import javax.swing.JFrame;

/**
 * ReusableTreeViewDialog
 
 Implementación del GUI con navegación mediante vista de árbol
 *
 * @author Marcos Martínez Poladura
 */
public class ReusableTreeViewDialog extends ReusableExtensibleDialog {
    /**
     * Nombre del árbol
     */
    private String navigatName;

    /**
     * Constructor
     * 
     * @param gui el gui que inicializa el dialogo
     * @param caption el nombre del contenedor
     */
    public ReusableTreeViewDialog(ReusableDialog gui, String caption) {
        super();
        this.gui = gui;
        this.contName = caption;
    }
    
    /**
     * Inicializar diálogo
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario
     */
    public Boolean setUpDialog() {
        return false;
    }

    public void setTreeNavigatorName(String navigatName) {
        this.navigatName = navigatName;
    }
    
    public String getTreeNavigatorName() {
        return this.navigatName;
    }
}
