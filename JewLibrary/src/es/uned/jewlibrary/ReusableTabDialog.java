package es.uned.jewlibrary;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

/**
 * ReusableTabDialog
 
 Implementación del GUI con navegación mediante pestañas
 *
 * @author Marcos Martínez Poladura
 */
public class ReusableTabDialog extends ReusableExtensibleDialog {

    /** 
     * Panel contenedor
     */
    private JTabbedPane principal;
        
    /**
     * Constructor
     * 
     * @param gui el gui que inicializa el dialogo
     * @param caption el nombre del contenedor
     */
    public ReusableTabDialog(ReusableDialog gui, String caption) {
        super();
        this.gui = gui;
        this.contName = caption;
    }

    /**
     * Inicializar diálogo
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario
     */
    @Override
    public Boolean setUpDialog() {
        this.principal = new JTabbedPane();
        return true;
    }  
}
