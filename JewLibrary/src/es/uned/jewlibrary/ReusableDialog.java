package es.uned.jewlibrary;

import java.util.ArrayList;
import javax.swing.JFrame;

/**
 * ReusableDialog
 * 
 * @author Marcos Martínez Poladura
 */
public abstract class ReusableDialog extends JFrame {
    
    /**
     * contiene la representación del GUI
     */
    protected ReusableDialog gui = null;
    
    /**
     * contiene la clase a validar
     */
    protected Object extension = null;

    /**
     * Añade un javabean relacionado con el interfaz
     * 
     * @param Object extension el javabean que se relaciona con el interfaz
     */
    public void setExtension(Object extension) {
        this.extension = extension;
    }
    
    /**
     * Validación del contenido de las páginas, cuando se pulsa el botón aceptar
     */
    protected void accept() {
        try {
            for(var child: gui.getChildrenList()) {
                child.validate();
            }
            for(var child: gui.getChildrenList()) {
                child.save();
            }
            for(var child: gui.getChildrenList()) {
                child.clear();
            }
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Cancelación de los cambios en las páginas, cuando se pulsa el botón cancelar
     */
    protected void reject() {
        try {
            for(var child: gui.getChildrenList()) {
                child.clear();
            }
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
