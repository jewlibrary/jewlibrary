package es.uned.jewlibrary.application;

import es.uned.jewlibrary.exceptions.ReusableCleanException;
import es.uned.jewlibrary.exceptions.ReusableSaveException;
import es.uned.jewlibrary.exceptions.ReusableValidateException;

/**
 *
 * @author maquinaria
 */
public interface ReusableFormInterface {
    /**
     *  Validacion de los campos del diálogo, si devuelve verdadero se ejecutan
     *  los otros dos métodos, si no, se continúa la edición
     * @return TRUE si la validación es correcta, FALSE en caso contrario
     * @throws ReusableValidateException si hubo algún error
     */
    public void validateThis() throws ReusableValidateException;

    /**
     * Actualiza los campos de la clase editada desde los del diálogo
     * @throws ReusableSaveException si hubo algún error
     */
    public void saveThis() throws ReusableSaveException;

    /**
     * Realiza tareas de finalización, cierre de ficheros, etc.
     * @throws ReusableCleanException si hubo algún error
     */
    public void cleanThis() throws ReusableCleanException;
    
    /**
     * Emite un mensaje con una cadena de identificación y un valor
     * @param identificacion String cadena de identificación
     * @param valor int valor
     */
    public void cambiaVal(String identificacion, int valor);
    
    /**
     * Recibe mensajes con una cadena de identificación y un valor
     * @param identificacion String cadena de identificación
     * @param valor int valor
     */
    public void getExternVal(String identificacion, int valor);
}
