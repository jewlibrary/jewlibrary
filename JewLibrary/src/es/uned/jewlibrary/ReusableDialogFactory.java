package es.uned.jewlibrary;

import es.uned.jewlibrary.ReusableExtensibleDialog;
import java.lang.String;

/**
 * ReusableDialogFactory
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableDialogFactory {
    // Constantes que indican el tipo de contenedor
    static final int CONT_SIMPLE = 1;
    static final int CONT_TAB = 2;
    static final int CONT_TREEVIEW = 3;
    static final int CONT_TOOLBOX = 4;
    
    /**
     * Constructor
     */
    public ReusableDialogFactory() {
        super();
    }

    /**
     * Crea un nuevo contenedor
     * 
     * @param contType int tipo de contenedor
     * @param wParent JFrame contenedor padre
     * @param name String nombre del contenedor
     * @return diálogo creado
     */
    public ReusableDialog createDialog(int contType, ReusableDialog wParent, String name) {
        ReusableDialog contenedor = null;
        switch (contType) {
            case CONT_SIMPLE: 
                contenedor = new ReusableSimpleDialog(wParent, name);
                break;
            case CONT_TAB: 
                contenedor = new ReusableTabDialog(wParent, name);
                break;
            case CONT_TREEVIEW: 
                contenedor = new ReusableTreeViewDialog(wParent, name);
                break;
            default:
        }
        return contenedor;
    }

}