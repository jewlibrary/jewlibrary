/*
 * Copyright (C) 2021 Marcos Martínez Poladura
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.widget;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import javax.swing.JLabel;

/**
 * Componente JDial, equivalente al componente originario de QT
 * 
 * @author Marcos Martínez Poladura
 */
public class JDial extends JLabel {

    /**
     * Valor en modo texto del valor del dial
     */
    private String text;
    
    /**
     * Ángulo en grados del componente (0 - 360)
     */
    private double angle;
    
    /**
     * Valor del JDial
     */
    private int value;
    
    /**
     * Constructor de JSpinnerCircle, componente de tipo spinner circular.
     * Se inicializa con el valor 0.
     */
    public JDial() {
        super();
        this.setEnabled(true);
        this.setValue(0);
        setMinimumSize(new Dimension(50,50));
        setPreferredSize(new Dimension(50,50));
        setFocusable(true);

        this.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusGained(FocusEvent e) {
                        super.focusGained(e);
                    }
                });
        
        // Añade un evento para gestionar el teclado
        this.addKeyListener(new KeyAdapter() {
            /**
             * Gestión del teclado en el componente
             * 
             * Cuando se pulsa la tecla izquierda o abajo, el componente resta
             * una unidad.
             * Cuando se pulsa la tecla derecha o arriba, el componente suma
             * una unidad
             * 
             * @param e KeyEvent evento de teclado
             */
             @Override
            public void keyReleased(KeyEvent e) {
                int key = e.getKeyCode();
                JDial source = (JDial) e.getSource();

                if (key == KeyEvent.VK_KP_LEFT || key == KeyEvent.VK_LEFT 
                       || key == KeyEvent.VK_KP_DOWN || key == KeyEvent.VK_DOWN)
                {
                    source.setValue(source.getValue() - 1);

                }
                else if (key == KeyEvent.VK_KP_RIGHT || key == KeyEvent.VK_RIGHT 
                       || key == KeyEvent.VK_KP_UP || key == KeyEvent.VK_UP)
                {
                    source.setValue(source.getValue() + 1);
                }                
            }
        });
        
        // Añade un evento para gestionar el ratón
        this.addMouseListener(new MouseAdapter() {
            
               /**
                * Mueve el componente en la dirección indicada por el click del 
                * ratón
                * 
                * Cuando se pulsa el ratón dentro del componente, calcula el 
                * ángulo de la recta que va desde el centro al lugar pulsado, y 
                * orienta el componente en esa dirección
                * 
                * @param e MouseEvent evento de ratón
                */
               public void mousePressed(MouseEvent e) {
                   JDial dial = (JDial)(e.getSource());
                   if (!dial.hasFocus()) {
                       dial.requestFocus();
                   }
                   // Obtiene las coordenadas de la pulsación
                   int clickedX = e.getX();
                   int clickedY = e.getY();

                   // Obtiene el componente base
                   JDial source = (JDial) e.getSource();
                   
                   // Calcula el centro del componente
                   int centrox = source.getSize().width / 2;
                   int centroy = source.getSize().height / 2;
                   
                   // Calcula el desplazamiento del click respecto al centro
                   int delta_x = clickedX - centrox;
                   int delta_y = clickedY - centroy;
                   
                   // Calcula el ángulo entre la recta generada y el eje de 
                   // coordenadas
                   double angulo = Math.atan2(delta_y, delta_x);

                   // Pasa el ángulo de radianes a grados
                   angulo = Math.toDegrees(angulo); 
                   
                   if (angulo < 0) {
                       angulo = 360 + angulo;
                   }
                   // Establece el ángulo de la dirección en el componente
                   source.setAngle(angulo);
               }
        });
    }

    /**
     * Crea la imagen del componente spinner para valor 0
     * 
     * @param width int Ancho del spinner
     * @param height int Alto del spinner
     * @param foreground Color color de frente del spinner
     * @param background Color color de fondo del spinner
     * @param parentBackground Color color de fondo del contenedor del spinner
     * @param focus boolean indica si el componente tiene el foco o no, para 
     *              mostrarlo resaltado si es que lo tiene
     * @return BufferedImage imagen creada con los parámetros seleccionados
     */
    private static BufferedImage createImage(int width, int height, 
            Color foreground, Color background, Color parentBackground, 
            boolean focus) {
        BufferedImage img = 
                new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
        
        Graphics2D g2 = img.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_ON);

        int w4 = width >> 2; // w4 = width / 4
        int h2 = height >> 1; // h2 = height / 2
        int h4 = height >> 2; // h4 = height / 4
                
        g2.setColor(parentBackground);
        g2.drawRect(0,0,width,height);

        g2.setColor(background);
        
        // Si el componente tiene el foco, aumenta el grosor del círculo
        if (focus) {
            g2.setStroke(new BasicStroke(3));
        }
        g2.drawOval(0,0, width, height);
        g2.setColor(foreground);
        Polygon p = new Polygon(new int[] {w4, width, w4}, 
                new int[] {h4, h2, (3*h4)}, 3);
        g2.fillPolygon(p);
        g2.dispose();
        return img;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        
        int w = getWidth();
        int h = getHeight();

        BufferedImage img = JDial.createImage(w, h, getForeground(), 
                Color.WHITE, this.getParent().getBackground(), this.hasFocus());
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_ON);

        // Crea la imagen rotada el ángulo indicado en this.angle
        final BufferedImage rotatedImage = 
                new BufferedImage(w, h, BufferedImage.TRANSLUCENT);
        
        final AffineTransform at = new AffineTransform();
        at.translate(w / 2, h / 2);
        at.rotate(Math.toRadians(this.angle),0, 0);
        at.translate(-img.getWidth() >> 1, -img.getHeight() >> 1);
        
        final AffineTransformOp rotateOp = 
                new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        
        rotateOp.filter(img,rotatedImage);        
        g2.drawImage(rotatedImage, 0, 0, null);
        
        invalidate();
        validate();
        repaint();
    }

    /** 
     * Establece el valor del componente spinner, de 0 a 100
     * @param value int valor del spinner. Si es mayor que 100, maneja el ángulo 
     *              del módulo 100
     */
    public void setValue(int value) {
        this.value = value;
        value = value % 100;
        this.angle = (value * 360) / 100;
        this.setText("" + value);
    }
    
    /**
     * Devuelve el valor del componente spinner
     * 
     * @return int valor del componente spinner
     */
    public int getValue() {
        return this.value;
    }
    
    /** 
     * Establece el ángulo del componente spinner, en radianes
     * @param value double ángulo del spinner
     */
    private void setAngle(double angle) {
        this.angle = angle;
        this.value = (int)Math.round(this.angle / 3.6);
        this.setText("" + value);
    }

    /**
     * Establece el valor en modo texto
     * @param value String el entero en modo texto
     */
    @Override
    public void setText(String value) {
        try {
            int val = Integer.parseInt(value);
            this.value = val;
            val = val % 100;
            this.angle = (val * 360) / 100;
            this.text = "" + this.value;
            this.putClientProperty("text", this.text);
        }
        catch (Exception e) {
        }
    }

    /**
     * Devuelve el valor en modo texto
     * 
     * @return String entero en modo texto
     */
    @Override
    public String getText() {
        return this.text;
    }
}
