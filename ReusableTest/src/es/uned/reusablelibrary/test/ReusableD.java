/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

import es.uned.reusablelibrary.ReusableExtensibleDialog;
import es.uned.reusablelibrary.ReusableFormInterface;
import es.uned.reusablelibrary.exceptions.ChangeValueException;
import es.uned.reusablelibrary.exceptions.ReusableCleanException;
import es.uned.reusablelibrary.exceptions.ReusableSaveException;
import es.uned.reusablelibrary.exceptions.ReusableValidateException;
import es.uned.reusablelibrary.observer.ValueListener;
import java.util.List;

/**
 * Diálogo ReusableD, que extiende la clase de diseñador GUI_D implementando los métodos de ReusableFormInterface.
 * 
 * <p><img src="doc-files/ProgrammerSwingDialog_class.png" alt="Diálogo Swing del Programador"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableD extends es.uned.reusablelibrary.test.repository.GUI_D implements ReusableFormInterface
{    
    /**
     * Diálogo reusable extensible padre del actual.
     */
    private ReusableExtensibleDialog parent;

    /**
     * Clase D actual (POJO).
     * Se cargará con los valores del formulario cuando se pulse el botón ACEPTAR.
     */
    protected D classD = null;

    /**
     * Clase D cargada (POJO).
     * Es la clase de inicialización, se guarda aparte para poder resetear a sus valores.
     */
    protected D loadedClassD = null;
    
    /**
     * Cuenta el número de instancias de la clase, para identificarlas individualmente a efectos del patrón Observer.
     */
    private static int numInstances = 0;
    
    /**
     * Identificador de la instancia en particular.
     */
    private int instanceId = 0;

    /**
     * Constructor por defecto.
     */
    public ReusableD() {
        super();
        super.init();
        ReusableD.numInstances++;
        this.instanceId = ReusableD.numInstances;
    }

    /**
     * Constructor.
     * 
     * @param parent ReusableExtensibleDialog Diálogo reusable extensible padre del actual.
     */
    public ReusableD(ReusableExtensibleDialog parent) {
        super();
        super.init();
        this.parent = parent;
        ReusableD.numInstances++;
        this.instanceId = ReusableD.numInstances;
    }

    /**
     * Inicializar diálogo.
     * 
     * @param classD D instancia para inicializar el formulario.
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario.
     */
    public Boolean setUpDialog(D classD) {
        // Inicializa claseD.
        initialize(classD);
        return true;
    }
    
    /**
     * Inicialización de la clase D.
     * 
     * @param classD D instancia para inicializar el formulario.
     */
    private void initialize(D classD) {
        if (classD == null) {
            this.loadedClassD = new D();
        }
        else {
            this.loadedClassD = classD;
        }
        this.classD = this.loadedClassD;
        
        super.campo11.setText(this.classD.getCampo11());
        super.campo12.setValue(this.classD.getCampo12());
        super.campo13.setText(this.classD.getCampo13());
        super.campo14.setText(this.classD.getCampo14());
        super.campo15.setText(this.classD.getCampo15());
    }

    /**
     * Da de alta componentes para publicar sus valores mediante el patrón Observer y suscripciones a otros componentes para reproducir sus valores en los componentes indicados.
     * 
     * @param instances List lista de instancias a las que se le aplicará el patrón Observer para el paso de datos entre componentes.
     */
    public static void subscriptions(List<ReusableD> instances) {
        try {
            for (int i = 0, max = instances.size(); i < max; i++) {
                ReusableD instance = instances.get(i);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableD.campo11" + instance.instanceId, instance.campo11);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableD.campo12" + instance.instanceId, instance.campo12);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableD.campo13" + instance.instanceId, instance.campo13);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableD.campo14" + instance.instanceId, instance.campo14);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableD.campo15" + instance.instanceId, instance.campo15);
            }
            for (int i = 0, max = instances.size(); i < max; i++) {
                ReusableD instance = instances.get(i);
                for (int j = 1, max2 = instances.size(); j <= max2; j++) {
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableD.campo11" + j, new ValueListener(instance.campo11));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableD.campo12" + j, new ValueListener(instance.campo12));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableD.campo13" + j, new ValueListener(instance.campo13));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableD.campo14" + j, new ValueListener(instance.campo14));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableD.campo15" + j, new ValueListener(instance.campo15));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableB.campo2", new ValueListener(instance.campo12));
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Obtener campo 11.
     * 
     * @return String campo 11.
     */
    private String getCampo11() {
        return super.campo11.getText();
    }

    /**
     * Obtener campo 12.
     * 
     * @return int campo 12.
     */
    private int getCampo12() {
        return Integer.parseInt(super.campo12.getValue().toString());
    }
    
    /**
     * Obtener campo 13.
     * 
     * @return String campo 13.
     */
    private String getCampo13() {
        return super.campo13.getText();
    }
    
    /**
     * Obtener campo 14.
     * 
     * @return String campo 14.
     */
    private String getCampo14() {
        return new String(super.campo14.getPassword());
    }
    
    /**
     * Obtener campo 15.
     * 
     * @return String campo 15.
     */
    private String getCampo15() {
        return new String(super.campo15.getPassword());
    }
    
    /**
     * Validacion de los campos del diálogo, si devuelve verdadero se ejecutan los otros dos métodos, si no, se continúa la edición.
     * 
     * @return TRUE si la validación es correcta, FALSE en caso contrario.
     * @throws ReusableValidateException si hubo algún error.
     */
    @Override
    public Boolean validateThis() throws ReusableValidateException {
        String log = "ReusableD.validateThis()\n";
        log += "========================\n";
        try {
            String lcampo14 = getCampo14();
            String lcampo15 = getCampo15();

            if (lcampo14 == null || lcampo14.trim().length() == 0) {
                throw new ReusableValidateException("Clase D.Campo 14 no puede ser vacío");            
            }
            if (lcampo15 == null || lcampo15.trim().length() == 0) {
                throw new ReusableValidateException("Clase D.Campo 15 no puede ser vacío");
            }
            if (!lcampo15.equals(lcampo14)) {
                throw new ReusableValidateException("Clase D.Las contraseñas no coinciden");
            }
        }
        catch (ReusableValidateException e) {
            log += e.getLocalizedMessage() + "\n";
            System.out.println(log);
            throw new ReusableValidateException(e.getLocalizedMessage());
        }
        return true;
    }

    /**
     * Actualiza los campos de la clase editada desde los del diálogo.
     * 
     * @throws ReusableSaveException si hubo algún error.
     */
    @Override
    public void saveThis() throws ReusableSaveException {
        System.out.println("ReusableD.saveThis()");
        System.out.println("====================");
        try {
            classD.setCampo11(getCampo11());
            classD.setCampo12(getCampo12());
            classD.setCampo13(getCampo13());
            classD.setCampo14(getCampo14());
            classD.setCampo15(getCampo15());
            System.out.println(this.classD);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableSaveException(e.getLocalizedMessage());
        }
    }

    /**
     * Realiza tareas de finalización, cierre de ficheros, etc.
     * 
     * @throws ReusableCleanException si hubo algún error.
     */
    @Override
    public void cleanThis() throws ReusableCleanException {
        try {
            initialize(this.loadedClassD);
            System.out.println("ReusableD.cleanThis()");
            System.out.println("=====================");
            System.out.println(this.classD);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableCleanException(e.getLocalizedMessage());
        }
    }

    /**
     * Devuelve el contenido de la clase D con los valores actuales.
     * 
     * @return D la clase D con los valores actuales.
     */
    public D getClassD() {
        return this.classD;
    }
    
    /**
     * Emite un mensaje con una cadena de identificación y un valor.
     * 
     * @param identification String cadena de identificación.
     * @param value Object valor.
     * @throws ChangeValueException si hubo algún error.
     */
    @Override
    public void changeValue(String identification, Object value) throws ChangeValueException {
        try {
            ReusableExtensibleDialog.getEventManager().notify(identification, value);
        }
        catch (Exception e) {
            throw new ChangeValueException("Error al notificar un cambio de valor en " + identification + " al valor " + value + " en ReusableD");
        }
    }    
}