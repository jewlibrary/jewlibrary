/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

import es.uned.reusablelibrary.ReusableExtensibleDialog;
import es.uned.reusablelibrary.ReusableFormInterface;
import es.uned.reusablelibrary.exceptions.ChangeValueException;
import es.uned.reusablelibrary.exceptions.ReusableCleanException;
import es.uned.reusablelibrary.exceptions.ReusableSaveException;
import es.uned.reusablelibrary.exceptions.ReusableValidateException;
import es.uned.reusablelibrary.observer.ValueListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import javax.swing.AbstractButton;

/**
 * Diálogo ReusableB, que extiende la clase de diseñador GUI_B implementando los métodos de ReusableFormInterface.
 * 
 * <p><img src="doc-files/ProgrammerSwingDialog_class.png" alt="Diálogo Swing del Programador"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableB extends es.uned.reusablelibrary.test.repository.GUI_B implements ReusableFormInterface
{    
    /**
     * Diálogo reusable extensible padre del actual.
     */
    private ReusableExtensibleDialog parent;
    
    /**
     * Clase B cargada (POJO).
     * Es la clase de inicialización, se guarda aparte para poder resetear a sus valores.
     */
    protected B classB = null;

    /**
     * Clase B cargada (POJO).
     * Es la clase de inicialización, se guarda aparte para poder resetear a sus valores.
     */
    protected B loadedClassB = null;
    
    /**
     * Constructor por defecto.
     */
    public ReusableB() {
        super();
        super.init();
    }

    /**
     * Constructor.
     * 
     * @param parent ReusableExtensibleDialog Diálogo reusable extensible padre del actual.
     */
    public ReusableB(ReusableExtensibleDialog parent) {
        super();
        super.init();
        this.parent = parent;
    }

    /**
     * Inicializar diálogo.
     * 
     * @param classB B instancia para inicializar el formulario.
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario.
     */
    public Boolean setUpDialog(B classB) {
        try {
            // Inicializa claseB.
            initialize(classB);

            // Añade soporte para patrón observador.
            ReusableExtensibleDialog.getEventManager().addOperation("ReusableB.campo2", this.campo2);
            ReusableExtensibleDialog.getEventManager().subscribe("ReusableD.campo121", new ValueListener(super.campo2));

            return true;
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
            return false;
        }
    }
    
    /**
     * Inicialización de la clase B
     * 
     * @param classB B instancia para inicializar el formulario.
     */
    private void initialize(B classB) {
        if (classB == null) {
            this.loadedClassB = new B();
        }
        else {
            this.loadedClassB = classB;
        }
        this.classB = this.loadedClassB;
        
        if (this.classB.getCampo1() != null) {
            while(super.campo1.getElements().hasMoreElements()) {
                AbstractButton c1 = super.campo1.getElements().nextElement();
                if (c1.getText().equals(this.classB.getCampo1())) {
                    super.campo1.setSelected(c1.getModel(), true);
                    break;
                }
            }
        }
        super.campo2.setValue(this.classB.getCampo2());

        if (this.classB.getCampo3() != null)
            super.campo3.setSelectedItem(this.classB.getCampo3());
        
        super.campo4.setText(this.classB.getCampo4());
        super.campo5.setValue(this.classB.getCampo5());
    }

    private String getCampo1() {
        String ret = null;
        for (Enumeration<AbstractButton> buttons = super.campo1.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                ret = button.getText();
                break;
            }        
        }
        return ret;
    }

    private int getCampo2() {
        return super.campo2.getValue();
    }
    
    private String getCampo3() {
        return super.campo3.getSelectedItem().toString();
    }
    
    private String getCampo4() {
        return super.campo4.getText();
    }
    
    private Date getCampo5() {
        return (Date) super.campo5.getValue();
    }
    
    /**
     * Validacion de los campos del diálogo, si devuelve verdadero se ejecutan los otros dos métodos, si no, se continúa la edición.
     * 
     * @return TRUE si la validación es correcta, FALSE en caso contrario.
     * @throws ReusableValidateException si hubo algún error.
     */
    @Override
    public Boolean validateThis() throws ReusableValidateException {
        String log = "ReusableB.validateThis()\n";
        log +=       "========================\n";
        try {
            String lcampo4 = getCampo4();
            Date lcampo5 = getCampo5();

            if (lcampo4 == null || lcampo4.trim().length() == 0) {
                throw new ReusableValidateException("clase B.Campo 4 no puede ser vacío");            
            }
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            if (simpleDateFormat.format(lcampo5).compareTo(simpleDateFormat.format(new Date())) < 0 ) {
                throw new ReusableValidateException("clase B.La fecha de Campo 5 no puede ser anterior a la fecha actual");
            }
        }
        catch (ReusableValidateException e) {
            log += e.getLocalizedMessage() + "\n";
            System.out.println(log);
            throw new ReusableValidateException(e.getLocalizedMessage());
        }
        return true;
    }

    /**
     * Actualiza los campos de la clase editada desde los del diálogo.
     * 
     * @throws ReusableSaveException si hubo algún error.
     */
    @Override
    public void saveThis() throws ReusableSaveException {
        System.out.println("ReusableB.saveThis()");
        System.out.println("====================");
        try {
            this.classB.setCampo1(getCampo1());
            this.classB.setCampo2(getCampo2());
            this.classB.setCampo3(getCampo3());
            this.classB.setCampo4(getCampo4());
            this.classB.setCampo5(getCampo5());     
            System.out.println(this.classB);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableSaveException(e.getLocalizedMessage());
        }
    }

    /**
     * Realiza tareas de finalización, cierre de ficheros, etc.
     * 
     * @throws ReusableCleanException si hubo algún error.
     */
    @Override
    public void cleanThis() throws ReusableCleanException {
        try {
            initialize(this.loadedClassB);
            System.out.println("ReusableB.cleanThis()");
            System.out.println("=====================");
            System.out.println(this.classB);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableCleanException(e.getLocalizedMessage());
        }
    }

    /**
     * Devuelve el contenido de la clase B con los valores actuales
     * 
     * @return B la clase B con los valores actuales
     */
    public B getClassB() {
        return this.classB;
    }
    
    /**
     * Emite un mensaje con una cadena de identificación y un valor.
     * 
     * @param identification String cadena de identificación.
     * @param value Object valor.
     * @throws ChangeValueException si hubo algún error.
     */
    @Override
    public void changeValue(String identification, Object value) throws ChangeValueException {
        try {
            ReusableExtensibleDialog.getEventManager().notify(identification, value);
        }
        catch (Exception e) {
            throw new ChangeValueException("Error al notificar un cambio de valor en " + identification + " al valor " + value + " en ReusableB");
        }
    }    
}
