/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

import es.uned.reusablelibrary.ReusableExtensibleDialog;
import es.uned.reusablelibrary.ReusableFormInterface;
import es.uned.reusablelibrary.exceptions.ChangeValueException;
import es.uned.reusablelibrary.exceptions.ReusableCleanException;
import es.uned.reusablelibrary.exceptions.ReusableSaveException;
import es.uned.reusablelibrary.exceptions.ReusableValidateException;
import es.uned.reusablelibrary.observer.ValueListener;
import java.util.Enumeration;
import java.util.List;
import javax.swing.AbstractButton;

/**
 * Diálogo ReusableA, que extiende la clase de diseñador GUI_A implementando los métodos de ReusableFormInterface.
 * 
 * <p><img src="doc-files/ProgrammerSwingDialog_class.png" alt="Diálogo Swing del Programador"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableA extends es.uned.reusablelibrary.test.repository.GUI_A implements ReusableFormInterface
{    
    /**
     * Diálogo reusable extensible padre del actual.
     */
    private ReusableExtensibleDialog parent;

    /**
     * Clase A actual (POJO).
     * Se cargará con los valores del formulario cuando se pulse el botón ACEPTAR.
     */
    protected A classA = null;

    /**
     * Clase A cargada (POJO).
     * Es la clase de inicialización, se guarda aparte para poder resetear a sus valores.
     */
    protected A loadedClassA = null;
    
    /**
     * Cuenta el número de instancias de la clase, para identificarlas individualmente a efectos del patrón Observer.
     */
    private static int numInstances = 0;
    
    /**
     * Identificador de la instancia en particular.
     */
    private int instanceId = 0;
    
    /**
     * Constructor por defecto.
     */
    public ReusableA() {
        super();
        super.init();
        ReusableA.numInstances++;
        this.instanceId = ReusableA.numInstances;
    }

    /**
     * Constructor.
     * 
     * @param parent ReusableExtensibleDialog Diálogo reusable extensible padre del actual.
     */
    public ReusableA(ReusableExtensibleDialog parent) {
        super();
        super.init();
        this.parent = parent;
        ReusableA.numInstances++;
        this.instanceId = ReusableA.numInstances;
    }

    /**
     * Inicializar diálogo.
     * 
     * @param classA A instancia para inicializar el formulario.
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario.
     */
    public Boolean setUpDialog(A classA) {
        // Inicializa classA.
        this.initialize(classA);
                        
        return true;
    }

    /**
     * Da de alta componentes para publicar sus valores mediante el patrón Observer y suscripciones a otros componentes para reproducir sus valores en los componentes indicados.
     * 
     * @param instances List lista de instancias a las que se le aplicará el patrón Observer para el paso de datos entre componentes.
     */
    public static void subscriptions(List<ReusableA> instances) {
        try {
            for (int i = 0, max = instances.size(); i < max; i++) {
                ReusableA instance = instances.get(i);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA.nombre" + instance.instanceId, instance.nombre);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA.apellidos" + instance.instanceId, instance.apellidos);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA.dni" + instance.instanceId, instance.dni);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA.estadoCivil" + instance.instanceId, instance.estadoCivil);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA.hombre" + instance.instanceId, instance.hombre);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA.mujer" + instance.instanceId, instance.mujer);
            }
            for (int i = 0, max = instances.size(); i < max; i++) {
                ReusableA instance = instances.get(i);
                for (int j = 1, max2 = instances.size(); j <= max2; j++) {
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA.nombre" + j, new ValueListener(instance.nombre));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA.apellidos" + j, new ValueListener(instance.apellidos));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA.dni" + j, new ValueListener(instance.dni));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA.estadoCivil" + j, new ValueListener(instance.estadoCivil));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA.hombre" + j, new ValueListener(instance.hombre));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA.mujer" + j, new ValueListener(instance.mujer));
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }
    
    /**
     * Inicialización de la clase A.
     * 
     * @param classA A instancia para inicializar el formulario.
     */
    private void initialize(A classA) {
        if (classA == null) {
            this.loadedClassA = new A();
        }
        else {
            this.loadedClassA = classA;
        }
        this.classA = this.loadedClassA;

        // Inicializa los campos de la GUI.
        if (this.classA.getNombre() != null)
            super.nombre.setText(this.classA.getNombre());
        if (this.classA.getApellidos() != null)
            super.apellidos.setText(this.classA.getApellidos());
        if (this.classA.getDni() != null)
            super.dni.setText(this.classA.getDni());
        if (this.classA.getEstadoCivil() != null)
            super.estadoCivil.setSelectedItem(this.classA.getEstadoCivil());
        if (this.classA.getSexo() != null) {
            while(super.sexo.getElements().hasMoreElements()) {
                AbstractButton c1 = super.sexo.getElements().nextElement();
                if (c1.getText().equals(this.classA.getSexo())) {
                    super.sexo.setSelected(c1.getModel(), true);
                    break;
                }
            }
        }
    }

    /**
     * Obtiene el nombre.
     * 
     * @return String nombre.
     */
    private String getNombre() {
        return super.nombre.getText();
    }

    /**
     * Obtiene los apellidos.
     * 
     * @return String apellidos.
     */
    private String getApellidos() {
        return super.apellidos.getText();
    }

    /**
     * Obtiene el DNI.
     * 
     * @return String DNI.
     */
    private String getDni() {
        return super.dni.getText();
    }
    
    /**
     * Obtiene el estado civil.
     * 
     * @return String estado civil.
     */
    private String getEstadoCivil() {
        return super.estadoCivil.getSelectedItem().toString();
    }

    /**
     * Obtiene el sexo.
     * 
     * @return String sexo.
     */
    private String getSexo() {
        String ret = null;
        for (Enumeration<AbstractButton> buttons = super.sexo.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                ret = button.getText();
                break;
            }        
        }
        return ret;
    }
    
    /**
     * Validacion de los campos del diálogo, si devuelve verdadero se ejecutan los otros dos métodos, si no, se continúa la edición.
     * 
     * @return TRUE si la validación es correcta, FALSE en caso contrario.
     * @throws ReusableValidateException si hubo algún error.
     */
    @Override
    public Boolean validateThis() throws ReusableValidateException {
        String log = "ReusableA.validateThis()\n";
        log +=       "========================\n";
        String lnombre = getNombre();
        String lapellidos = getApellidos();
        String ldni = getDni();
        String lestadoCivil = getEstadoCivil();
        String lsexo = getSexo();

        try {
            if (lnombre == null || lnombre.trim().length() == 0) {
                super.nombre.requestFocus();
                throw new ReusableValidateException("Clase A.Campo Nombre no puede estar vacío");            
            }
            if (lapellidos == null || lapellidos.trim().length() == 0) {
                super.apellidos.requestFocus();
                throw new ReusableValidateException("Clase A.Campo Apellidos no puede estar vacío");            
            }
            if (ldni == null || ldni.trim().length() == 0) {
                super.dni.requestFocus();
                throw new ReusableValidateException("Clase A.Campo DNI no puede estar vacío");
            }
            if (!validateDNI(ldni.trim())) {
                super.dni.requestFocus();
                throw new ReusableValidateException("Clase A.Campo DNI mal formado");
            }
            if (lestadoCivil == null || lestadoCivil.trim().length() == 0) {
                super.estadoCivil.requestFocus();
                throw new ReusableValidateException("Clase A.Campo Estado Civil no puede estar vacío");
            }
            if (lsexo == null || lsexo.trim().length() == 0) {
                throw new ReusableValidateException("Clase A.Campo Sexo no puede estar vacío");            
            }
        }
        catch (ReusableValidateException e) {
            log += e.getLocalizedMessage() + "\n";
            System.out.println(log);
            throw new ReusableValidateException(e.getLocalizedMessage());
           
        }
        return true;
    }

    /**
     * Validación de un número de DNI.
     * 
     * @param dni String el DNI que se quiere validar.
     * @return true si es válido, false si no.
     */
    private Boolean validateDNI(String dni) {
        dni = dni.toUpperCase();
        if (dni.length() < 9)
            dni = "000000000" + dni;
        dni = dni.substring(dni.length() - 9);
        String letter = "";
        
        try {
            int myDNI = Integer.parseInt(dni.substring(0,8));
            int remainder = 0;
            String[] letterAssign = {"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"};
            remainder = myDNI % 23;
            letter = letterAssign[remainder];
        }
        catch (Exception e) {
            return false;
        }
        if (letter.equals(dni.substring(8)))
            return true;
        return false;
    }
    
    /**
     * Actualiza los campos de la clase editada desde los del diálogo.
     * 
     * @throws ReusableSaveException si hubo algún error.
     */
    @Override
    public void saveThis() throws ReusableSaveException {
        System.out.println("ReusableA.saveThis()");
        System.out.println("====================");
        try {
            this.classA.setNombre(getNombre());
            this.classA.setApellidos(getApellidos());
            this.classA.setDni(getDni());
            this.classA.setEstadoCivil(getEstadoCivil());
            this.classA.setSexo(getSexo());
            System.out.println(this.classA);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableSaveException(e.getLocalizedMessage());
        }
    }

    /**
     * Realiza tareas de finalización, cierre de ficheros, etc.
     * 
     * @throws ReusableCleanException si hubo algún error.
     */
    @Override
    public void cleanThis() throws ReusableCleanException {
        try {
            initialize(this.loadedClassA);
            System.out.println("ReusableA.cleanThis()");
            System.out.println("=====================");
            System.out.println(this.classA);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableCleanException(e.getLocalizedMessage());
        }
    }

    /**
     * Devuelve el contenido de la clase A con los valores actuales.
     * 
     * @return A la clase A con los valores actuales.
     */
    public A getClassA() {
        return this.classA;
    }
    
    /**
     * Emite un mensaje con una cadena de identificación y un valor.
     * 
     * @param identification String cadena de identificación.
     * @param value Object valor.
     * @throws ChangeValueException si hubo algún error.
     */
    @Override
    public void changeValue(String identification, Object value) throws ChangeValueException {
        try {
            ReusableExtensibleDialog.getEventManager().notify(identification, value);
        }
        catch (Exception e) {
            throw new ChangeValueException("Error al notificar un cambio de valor en " + identification + " al valor " + value + " en ReusableA");
        }
    }    
}