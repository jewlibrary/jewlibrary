/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

import es.uned.reusablelibrary.DialogResizer;
import es.uned.reusablelibrary.ReusableDialogFactory;
import es.uned.reusablelibrary.ReusableExtensibleDialog;
import es.uned.reusablelibrary.ReusableTreeViewDialog;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JFrame;

/**
 * Aplicación de test.
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class TestApplication {

    /**
     * Programa de prueba, según el esquema definido en la especificación.
     */
    private static void testApp() {
        try {
            // Instancias que extienden las GUIs.
            ReusableA rA = null;
            ReusableA rA0 = null;
            ReusableA rA_A = null;
            ReusableA1 rA_A1 = null;
            ReusableA2 rA_A2 = null;
            ReusableA1 rA1S = null;
            ReusableA2 rA2S = null;
            ReusableB rB = null;
            ReusableC rC = null;
            ReusableC rCS = null;
            ReusableD rD = null;
            ReusableD rD1 = null;
            
            // Listas de componentes para montar el patrón Observer.
            List<ReusableA> listA = new ArrayList<ReusableA>();
            List<ReusableA1> listA1 = new ArrayList<ReusableA1>();
            List<ReusableA2> listA2 = new ArrayList<ReusableA2>();
            List<ReusableC> listC = new ArrayList<ReusableC>();
            List<ReusableD> listD = new ArrayList<ReusableD>();
            
            // Inicialización de contenidos.
            A classA = new A();
            A1 classA1 = new A1();
            A2 classA2 = new A2();
            B classB = new B();
            C classC = new C();
            D classD = new D();
            
            // Inicializa las clases compuestas.
            classA1.init(classA);
            classA2.init(classA);
            classB.init(classA);
            classB.init(classA1);
            classB.init(classA2);
            classB.init(classC);
            classC.init(classD);

            boolean result = false;
            
            // Diálogo para la clase A, es una GUI única. TIPO SIMPLE (VERTICAL).
            ReusableExtensibleDialog dialog_A =  ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A");
            if (dialog_A.setUpDialog()) {
                rA0 = new ReusableA(dialog_A);
                if (rA0.setUpDialog(classA)) {
                    listA.add(rA0);
                    dialog_A.append(rA0, "clase A");
                    result = true;
                }
            }
            
            if (!result) {
                new Exception("Error al inicializar dialog_A.");
            }
            
            // Diálogo para la clase A1, que contiene también la A. Tipo SIMPLE VERTICAL.
            ReusableExtensibleDialog dialog_A1 = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A1");
            result = false;
            if (dialog_A1.setUpDialog()) {
                rA = new ReusableA(dialog_A1);
                if (rA.setUpDialog(classA)) {
                    rA_A1 = new ReusableA1(dialog_A1);
                    if (rA_A1.setUpDialog(classA1)) {
                        listA.add(rA);
                        listA1.add(rA_A1);
                        dialog_A1.append(rA, "clase A");
                        dialog_A1.append(rA_A1, "clase A1");
                        result = true;
                    }
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_A1.");
            }

            // Diálogo para la clase A2, que contiene también la A. Tipo TAB.
            ReusableExtensibleDialog dialog_A2 = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_TAB, ReusableDialogFactory.LayoutType.NONE, null, "Test A2");
            result = false;
            if (dialog_A2.setUpDialog()) {
                rA_A = new ReusableA(dialog_A2);
                if (rA_A.setUpDialog(classA)) {
                    rA_A2 = new ReusableA2(dialog_A2);
                    if (rA_A2.setUpDialog(classA2)) {
                        listA.add(rA_A);
                        listA2.add(rA_A2);
                        dialog_A2.append(rA_A, "clase A");
                        dialog_A2.append(rA_A2, "clase A2");
                        result = true;
                    }
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_A2.");
            }

            // Diálogo para la clase A1 sola, tipo SIMPLE VERTICAL.
            ReusableExtensibleDialog dialog_A1S = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A1");
            result = false;
            if (dialog_A1S.setUpDialog()) {
                rA1S = new ReusableA1(dialog_A1S);
                if (rA1S.setUpDialog(classA1)) {
                    listA1.add(rA1S);
                    dialog_A1S.append(rA1S, "clase A1");
                    result = true;
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_A1S.");
            }

            // Diálogo para la clase A2 sola, tipo SIMPLE VERTICAL.
            ReusableExtensibleDialog dialog_A2S = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A1");
            result = false;
            if (dialog_A2S.setUpDialog()) {
                rA2S = new ReusableA2(dialog_A2S);
                if (rA2S.setUpDialog(classA2)) {
                    listA2.add(rA2S);
                    dialog_A2S.append(rA2S, "clase A2");
                    result = true;
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_A1S.");
            }
            
            // Diálogo para la clase C, que contiene también la D. Tipo SIMPLE HORIZONTAL.
            ReusableExtensibleDialog dialog_C = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_HORIZONTAL, null, "Test C");
            result = false;
            if (dialog_C.setUpDialog()) {
                rD = new ReusableD(dialog_C);
                if (rD.setUpDialog(classD)) {
                    rC = new ReusableC(dialog_C);
                    if (rC.setUpDialog(classC)) {
                        listC.add(rC);
                        listD.add(rD);
                        dialog_C.append(rC, "clase C");
                        dialog_C.append(rD, "clase D");
                        result = true;
                    }
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_C.");
            }

            // Diálogo para la clase C. Tipo SIMPLE (HORIZONTAL).
            ReusableExtensibleDialog dialog_CS = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_HORIZONTAL, null, "Test C");
            result = false;
            if (dialog_CS.setUpDialog()) {
                rCS = new ReusableC(dialog_CS);
                if (rCS.setUpDialog(classC)) {
                    listC.add(rCS);
                    dialog_CS.append(rCS, "clase C");
                    result = true;
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_CS.");
            }

            // Diálogo para la clase D. Tipo SIMPLE (VERTICAL).
            ReusableExtensibleDialog dialog_D = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test D");
            result = true;
            if (dialog_D.setUpDialog()) {
                rD1 = new ReusableD(dialog_D);
                if (rD1.setUpDialog(classD)) {
                    listD.add(rD);                
                    dialog_D.append(rD1, "clase D");
                    result = true;
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_D.");
            }

            // Diálogo para la clase B, que contiene a todas las demás. Tipo TREEVIEW.
            ReusableExtensibleDialog dialog_B = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_TREEVIEW, ReusableDialogFactory.LayoutType.NONE, null, "Raíz del árbol", "Test B");
            result = false;
            if (dialog_B.setUpDialog()) {
                rB = new ReusableB(dialog_B);
                if (rB.setUpDialog(classB)) {
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_A, "clase A");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_A1, "clase A1");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_A2, "clase A2");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_A1S, "clase A1 sola");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_A2S, "clase A2 sola");
                    ((ReusableTreeViewDialog)dialog_B).append(rB, "clase B");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_C, "clase C");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_CS, "clase C sola");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_D, "clase D");
                    result = true;
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_B.");
            }

            // Suscripción a los componentes que utilizan el patrón Observer.
            ReusableA.subscriptions(listA);
            ReusableA1.subscriptions(listA1);
            ReusableA2.subscriptions(listA2);
            ReusableC.subscriptions(listC);
            ReusableD.subscriptions(listD);

            // Lanza la aplicación
            JFrame frame = dialog_B.exec("Test App");
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Programa de prueba, según el esquema definido en la especificación.
     */
    private static void testAppRelleno() {
        try {
            // Instancias que extienden las GUIs.
            ReusableA rA = null;
            ReusableA rA0 = null;
            ReusableA rA_A = null;
            ReusableA1 rA_A1 = null;
            ReusableA2 rA_A2 = null;
            ReusableA1 rA1S = null;
            ReusableA2 rA2S = null;
            ReusableB rB = null;
            ReusableC rC = null;
            ReusableC rCS = null;
            ReusableD rD = null;
            ReusableD rD1 = null;
            
            // Listas de componentes para montar el patrón Observer.
            List<ReusableA> listA = new ArrayList<ReusableA>();
            List<ReusableA1> listA1 = new ArrayList<ReusableA1>();
            List<ReusableA2> listA2 = new ArrayList<ReusableA2>();
            List<ReusableC> listC = new ArrayList<ReusableC>();
            List<ReusableD> listD = new ArrayList<ReusableD>();
            
            // Inicialización de contenidos.
            A classA = new A();
            A1 classA1 = new A1();
            A2 classA2 = new A2();
            B classB = new B();
            C classC = new C();
            D classD = new D();
            
            // Se pueden poner campos por defecto, estableciéndolo en la clase.
            classA.setNombre("Marcos");
            classA.setApellidos("Martínez Poladura");
            classA.setDni("10899841B");
            classA.setSexo("Hombre");
            classA.setEstadoCivil("Soltero");

            classA1.setCalidad("Media");
            classA1.setVelocidad(15);
            
            classA2.setDireccion("Travesía Maldonado");
            classA2.setNum("1");
            classA2.setPiso("2");
            classA2.setPuerta("A");
            classA2.setCodigoPostal("16123");
            classA2.setLocalidad("Arcas");
            classA2.setProvincia("Cuenca");
            
            classB.setCampo1("campo 1");
            classB.setCampo2(44);
            classB.setCampo3("campo 3");
            classB.setCampo4("campo 4");
            classB.setCampo5(new Date());
            
            classC.setCampo6("campo 6");
            classC.setCampo7(44);
            classC.setCampo8("16543");
            classC.setCampo9("campo 9");
            classC.setCampo10("campo 10");
            
            classD.setCampo11("campo 11");
            classD.setCampo12(44);
            classD.setCampo13("campo13");
            classD.setCampo14("contraseña");
            classD.setCampo15("contraseña");
            
            // Inicializa las clases compuestas.
            classA1.init(classA);
            classA2.init(classA);
            classB.init(classA);
            classB.init(classA1);
            classB.init(classA2);
            classB.init(classC);
            classC.init(classD);

            boolean result = false;
            
            // Diálogo para la clase A, es una GUI única. TIPO SIMPLE (VERTICAL).
            ReusableExtensibleDialog dialog_A =  ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A");
            if (dialog_A.setUpDialog()) {
                rA0 = new ReusableA(dialog_A);
                if (rA0.setUpDialog(classA)) {
                    listA.add(rA0);
                    dialog_A.append(rA0, "clase A");
                    result = true;
                }
            }
            
            if (!result) {
                new Exception("Error al inicializar dialog_A.");
            }
            
            // Diálogo para la clase A1, que contiene también la A. Tipo SIMPLE VERTICAL.
            ReusableExtensibleDialog dialog_A1 = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A1");
            result = false;
            if (dialog_A1.setUpDialog()) {
                rA = new ReusableA(dialog_A1);
                if (rA.setUpDialog(classA)) {
                    rA_A1 = new ReusableA1(dialog_A1);
                    if (rA_A1.setUpDialog(classA1)) {
                        listA.add(rA);
                        listA1.add(rA_A1);
                        dialog_A1.append(rA, "clase A");
                        dialog_A1.append(rA_A1, "clase A1");
                        result = true;
                    }
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_A1.");
            }

            // Diálogo para la clase A2, que contiene también la A. Tipo TAB.
            ReusableExtensibleDialog dialog_A2 = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_TAB, ReusableDialogFactory.LayoutType.NONE, null, "Test A2");
            result = false;
            if (dialog_A2.setUpDialog()) {
                rA_A = new ReusableA(dialog_A2);
                if (rA_A.setUpDialog(classA)) {
                    rA_A2 = new ReusableA2(dialog_A2);
                    if (rA_A2.setUpDialog(classA2)) {
                        listA.add(rA_A);
                        listA2.add(rA_A2);
                        dialog_A2.append(rA_A, "clase A");
                        dialog_A2.append(rA_A2, "clase A2");
                        result = true;
                    }
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_A2.");
            }

            // Diálogo para la clase A1 sola, tipo SIMPLE VERTICAL.
            ReusableExtensibleDialog dialog_A1S = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A1");
            result = false;
            if (dialog_A1S.setUpDialog()) {
                rA1S = new ReusableA1(dialog_A1S);
                if (rA1S.setUpDialog(classA1)) {
                    listA1.add(rA1S);
                    dialog_A1S.append(rA1S, "clase A1");
                    result = true;
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_A1S.");
            }

            // Diálogo para la clase A2 sola, tipo SIMPLE VERTICAL.
            ReusableExtensibleDialog dialog_A2S = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A1");
            result = false;
            if (dialog_A2S.setUpDialog()) {
                rA2S = new ReusableA2(dialog_A2S);
                if (rA2S.setUpDialog(classA2)) {
                    listA2.add(rA2S);
                    dialog_A2S.append(rA2S, "clase A2");
                    result = true;
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_A1S.");
            }
            
            // Diálogo para la clase C, que contiene también la D. Tipo SIMPLE HORIZONTAL.
            ReusableExtensibleDialog dialog_C = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_HORIZONTAL, null, "Test C");
            result = false;
            if (dialog_C.setUpDialog()) {
                rD = new ReusableD(dialog_C);
                if (rD.setUpDialog(classD)) {
                    rC = new ReusableC(dialog_C);
                    if (rC.setUpDialog(classC)) {
                        listC.add(rC);
                        listD.add(rD);
                        dialog_C.append(rC, "clase C");
                        dialog_C.append(rD, "clase D");
                        result = true;
                    }
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_C.");
            }

            // Diálogo para la clase C. Tipo SIMPLE (HORIZONTAL).
            ReusableExtensibleDialog dialog_CS = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_HORIZONTAL, null, "Test C");
            result = false;
            if (dialog_CS.setUpDialog()) {
                rCS = new ReusableC(dialog_CS);
                if (rCS.setUpDialog(classC)) {
                    listC.add(rCS);
                    dialog_CS.append(rCS, "clase C");
                    result = true;
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_CS.");
            }

            // Diálogo para la clase D. Tipo SIMPLE (VERTICAL).
            ReusableExtensibleDialog dialog_D = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test D");
            result = true;
            if (dialog_D.setUpDialog()) {
                rD1 = new ReusableD(dialog_D);
                if (rD1.setUpDialog(classD)) {
                    listD.add(rD);                
                    dialog_D.append(rD1, "clase D");
                    result = true;
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_D.");
            }

            // Diálogo para la clase B, que contiene a todas las demás. Tipo TREEVIEW.
            ReusableExtensibleDialog dialog_B = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_TREEVIEW, ReusableDialogFactory.LayoutType.NONE, null, "Raíz del árbol", "Test B");
            result = false;
            if (dialog_B.setUpDialog()) {
                rB = new ReusableB(dialog_B);
                if (rB.setUpDialog(classB)) {
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_A, "clase A");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_A1, "clase A1");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_A2, "clase A2");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_A1S, "clase A1 sola");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_A2S, "clase A2 sola");
                    ((ReusableTreeViewDialog)dialog_B).append(rB, "clase B");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_C, "clase C");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_CS, "clase C sola");
                    ((ReusableTreeViewDialog)dialog_B).addExtensibleChild(dialog_D, "clase D");
                    result = true;
                }
            }
            if (!result) {
                throw new Exception("Error al inicializar dialog_B.");
            }

            // Suscripción a los componentes que utilizan el patrón Observer.
            ReusableA.subscriptions(listA);
            ReusableA1.subscriptions(listA1);
            ReusableA2.subscriptions(listA2);
            ReusableC.subscriptions(listC);
            ReusableD.subscriptions(listD);

            // Lanza la aplicación
            JFrame frame = dialog_B.exec("Test App");
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
 
    /**
     * Método principal, donde arranca la aplicación.
     * 
     * @param args String[] argumentos de inicialización, en este caso vacío.
     */
    public static void main(String[] args) {
        TestApplication.testApp();
        //TestApplication.testAppRelleno();
    }    
}