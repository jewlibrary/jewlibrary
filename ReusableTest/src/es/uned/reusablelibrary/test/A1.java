/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under terms of GNU General Public License as published by
 * Free Software Foundation, either version 3 of License, or
 * (at your option) any later version.
 *
 * This program is distributed in hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

/**
 * Clase A1 de ejemplo, contiene la información del diálogo GUI_A1 en una clase POJO (Plain Old Java Object), que es una clase básica independiente que no pertenece a ningún framework.
 * 
 * <p><img src="doc-files/POJO_class.png" alt="POJO class"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class A1 extends A {

    /**
     * Constructor por defecto.
     */
    public A1() {
        super();
    }

    /**
     * Inicializa la clase con una clase de tipo A.
     * 
     * @param classA A clase con los valores iniciales.
     */
    public void init(A classA) {
        super.init(classA);
    }

    /**
     * Inicializa la clase con una clase de tipo A1.
     * 
     * @param classA1 A1 clase con los valores iniciales.
     */
    public void init(A1 classA1) {
        this.setNombre(classA1.getNombre());
        this.setApellidos(classA1.getApellidos());
        this.setDni(classA1.getDni());
        this.setSexo(classA1.getSexo());
        this.setEstadoCivil(classA1.getEstadoCivil());
        this.setVelocidad(classA1.getVelocidad());
        this.setCalidad(classA1.getCalidad());
    }
    
    /**
     * Obtener la velocidad.
     * 
     * @return velocidad int velocidad.
     */
    public int getVelocidad() {
        return velocidad;
    }

    /**
     * Establecer la velocidad.
     * 
     * @param velocidad int velocidad.
     */
    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }

    /**
     * Obtener la calidad.
     * 
     * @return calidad String calidad.
     */
    public String getCalidad() {
        return calidad;
    }

    /**
     * Establecer la calidad.
     * 
     * @param calidad String calidad.
     */
    public void setCalidad(String calidad) {
        this.calidad = calidad;
    }
    
    /**
     * Velocidad.
     */
    protected int velocidad = 0;
    
    /**
     * Calidad.
     */
    protected String calidad = "";

    /**
     * Devuelve la representación en formato de cadena de los valores de la clase.
     * 
     * @return String contenido de la clase.
     */
    public String toString() {
        String ret = "CLASE A1:\n--------\n";
        ret += "velocidad = " + velocidad + "\n";
        ret += "calidad = " + calidad + "\n";
        return ret;
    }
}