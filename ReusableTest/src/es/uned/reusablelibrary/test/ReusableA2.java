/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

import es.uned.reusablelibrary.ReusableExtensibleDialog;
import es.uned.reusablelibrary.ReusableFormInterface;
import es.uned.reusablelibrary.exceptions.ChangeValueException;
import es.uned.reusablelibrary.exceptions.ReusableCleanException;
import es.uned.reusablelibrary.exceptions.ReusableSaveException;
import es.uned.reusablelibrary.exceptions.ReusableValidateException;
import es.uned.reusablelibrary.observer.ValueListener;
import java.util.List;

/**
 * Diálogo ReusableA2, que extiende la clase de diseñador GUI_A2 implementando los métodos de ReusableFormInterface.
 * 
 * <p><img src="doc-files/ProgrammerSwingDialog_class.png" alt="Diálogo Swing del Programador"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableA2 extends es.uned.reusablelibrary.test.repository.GUI_A2 implements ReusableFormInterface
{    
    /**
     * Diálogo reusable extensible padre del actual.
     */
    private ReusableExtensibleDialog parent;
    
    /**
     * Clase A2 cargada (POJO).
     * Es la clase de inicialización, se guarda aparte para poder resetear a sus valores.
     */
    protected A2 classA2 = null;

    /**
     * Clase A2 cargada (POJO).
     * Es la clase de inicialización, se guarda aparte para poder resetear a sus valores.
     */
    protected A2 loadedClassA2 = null;
    
    /**
     * Cuenta el número de instancias de la clase, para identificarlas individualmente a efectos del patrón Observer.
     */
    private static int numInstances = 0;
    
    /**
     * Identificador de la instancia en particular.
     */
    private int instanceId = 0;

    /**
     * Constructor por defecto.
     */
    public ReusableA2() {
        super();
        super.init();
        ReusableA2.numInstances++;
        this.instanceId = ReusableA2.numInstances;
   }
    
    /**
     * Constructor.
     * 
     * @param parent ReusableExtensibleDialog Diálogo reusable extensible padre del actual.
     */
    public ReusableA2(ReusableExtensibleDialog parent) {
        super();
        super.init();
        this.parent = parent;
        ReusableA2.numInstances++;
        this.instanceId = ReusableA2.numInstances;
    }

    /**
     * Inicializar diálogo.
     * 
     * @param classA2 A1 instancia para inicializar el formulario.
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario.
     */
    public Boolean setUpDialog(A2 classA2) {
        // Inicializa classA2.
        this.initialize(classA2);

        return true;
    }
    
    /**
     * Da de alta componentes para publicar sus valores mediante el patrón Observer y suscripciones a otros componentes para reproducir sus valores en los componentes indicados.
     * 
     * @param instances List lista de instancias a las que se le aplicará el patrón Observer para el paso de datos entre componentes.
     */
    public static void subscriptions(List<ReusableA2> instances) {
        try {
            for (int i = 0, max = instances.size(); i < max; i++) {
                ReusableA2 instance = instances.get(i);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA2.direccion" + instance.instanceId, instance.direccion);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA2.num" + instance.instanceId, instance.num);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA2.piso" + instance.instanceId, instance.piso);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA2.puerta" + instance.instanceId, instance.puerta);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA2.localidad" + instance.instanceId, instance.localidad);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA2.codigoPostal" + instance.instanceId, instance.codigoPostal);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA2.provincia" + instance.instanceId, instance.provincia);
            }
            for (int i = 0, max = instances.size(); i < max; i++) {
                ReusableA2 instance = instances.get(i);
                for (int j = 1, max2 = instances.size(); j <= max2; j++) {
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA2.direccion" + j, new ValueListener(instance.direccion));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA2.num" + j, new ValueListener(instance.num));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA2.piso" + j, new ValueListener(instance.piso));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA2.puerta" + j, new ValueListener(instance.puerta));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA2.localidad" + j, new ValueListener(instance.localidad));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA2.codigoPostal" + j, new ValueListener(instance.codigoPostal));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA2.provincia" + j, new ValueListener(instance.provincia));
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Inicialización de la clase A2.
     * 
     * @param classA2 A2 instancia para inicializar el formulario.
     */
    private void initialize(A2 classA2) {
        if (classA2 == null) {
            this.loadedClassA2 = new A2();
        }
        else {
            this.loadedClassA2 = classA2;
        }
        this.classA2 = this.loadedClassA2;

        if (this.classA2.getDireccion() != null)
            super.direccion.setText(this.classA2.getDireccion());
        if (this.classA2.getNum() != null)
            super.num.setText(this.classA2.getNum());
        if (this.classA2.getPiso() != null)
            super.piso.setText(this.classA2.getPiso());
        if (this.classA2.getPuerta() != null)
            super.puerta.setText(this.classA2.getPuerta());
        if (this.classA2.getCodigoPostal() != null)
            super.codigoPostal.setText(this.classA2.getCodigoPostal());
        if (this.classA2.getLocalidad() != null)
            super.localidad.setText(this.classA2.getLocalidad());
        if (this.classA2.getProvincia() != null)
           super.provincia.setSelectedItem(this.classA2.getProvincia());
    }

    /**
     * Obtener la dirección.
     * 
     * @return String dirección.
     */
    private String getDireccion() {
        return super.direccion.getText();
    }

    /**
     * Obtener el´número.
     * 
     * @return String número.
     */
    private String getNum() {
        return super.num.getText();
    }
        
    /**
     * Obtener el´piso.
     * 
     * @return String piso.
     */
    private String getPiso() {
        return super.piso.getText();
    }

    /**
     * Obtener la puerta.
     * 
     * @return String puerta.
     */
    private String getPuerta() {
        return super.puerta.getText();
    }
    
    /**
     * Obtener el´código postal.
     * 
     * @return String código postal.
     */
    private String getCodigoPostal() {
        return super.codigoPostal.getText();
    }

    /**
     * Obtener la localidad.
     * 
     * @return String localidad.
     */
    private String getLocalidad() {
        return super.localidad.getText();
    }

    /**
     * Obtener la provincia.
     * 
     * @return String provincia.
     */
    private String getProvincia() {
        return super.provincia.getSelectedItem().toString();
    }

    /**
     * Validacion de los campos del diálogo, si devuelve verdadero se ejecutan los otros dos métodos, si no, se continúa la edición.
     * 
     * @return TRUE si la validación es correcta, FALSE en caso contrario.
     * @throws ReusableValidateException si hubo algún error.
     */
    @Override
    public Boolean validateThis() throws ReusableValidateException {
        String log = "ReusableA2.validateThis()\n";
        log +=       "=========================\n";
        String ldireccion = getDireccion();
        String lcodigoPostal = getCodigoPostal();
        String llocalidad = getLocalidad();
        String lprovincia = getProvincia();
        try {
            if (ldireccion == null || ldireccion.trim().length() == 0) {
                super.direccion.requestFocus();
                throw new ReusableValidateException("Clase A2.Campo Dirección no puede estar vacío");            
            }
            if (lcodigoPostal == null || lcodigoPostal.trim().length() == 0) {
                super.codigoPostal.requestFocus();
                throw new ReusableValidateException("Clase A2.Campo Código Postal no puede estar vacío");            
            }
            if (llocalidad == null || llocalidad.trim().length() == 0) {
                super.localidad.requestFocus();
                throw new ReusableValidateException("Clase A2.Campo Localidad no puede estar vacío");            
            }
            if (lprovincia == null || lprovincia.trim().length() == 0) {
                super.provincia.requestFocus();
                throw new ReusableValidateException("Clase A2.Campo Provincia no puede estar vacío");            
            }
        }
        catch (ReusableValidateException e) {
            log += e.getLocalizedMessage() + "\n";
            System.out.println(log);
            throw new ReusableValidateException(e.getLocalizedMessage());
           
        }
        return true;
    }
    
    /**
     * Actualiza los campos de la clase editada desde los del diálogo.
     * 
     * @throws ReusableSaveException si hubo algún error.
     */
    @Override
    public void saveThis() throws ReusableSaveException {
        System.out.println("ReusableA2.saveThis()");
        System.out.println("=====================");
        try {
            classA2.setDireccion(getDireccion());
            classA2.setNum(getNum());
            classA2.setPiso(getPiso());
            classA2.setPuerta(getPuerta());
            classA2.setCodigoPostal(getCodigoPostal());
            classA2.setLocalidad(getLocalidad());
            classA2.setProvincia(getProvincia());
            System.out.println(classA2);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableSaveException(e.getLocalizedMessage());
        }
    }

    /**
     * Realiza tareas de finalización, cierre de ficheros, etc.
     * 
     * @throws ReusableCleanException si hubo algún error.
     */
    @Override
    public void cleanThis() throws ReusableCleanException {
        try {
            initialize(this.loadedClassA2);
            System.out.println("ReusableA2.cleanThis()");
            System.out.println("======================");
            System.out.println(this.classA2);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableCleanException(e.getLocalizedMessage());
        }
    }

    /**
     * Devuelve el contenido de la clase A2 con los valores actuales.
     * 
     * @return A2 la clase A2 con los valores actuales.
     */
    public A2 getClassA2() {
        return this.classA2;
    }
    
    /**
     * Emite un mensaje con una cadena de identificación y un valor.
     * 
     * @param identification String cadena de identificación.
     * @param value Object valor.
     * @throws ChangeValueException si hubo algún error.
     */
    @Override
    public void changeValue(String identification, Object value) throws ChangeValueException {
        try {
            ReusableExtensibleDialog.getEventManager().notify(identification, value);
        }
        catch (Exception e) {
            throw new ChangeValueException("Error al notificar un cambio de valor en " + identification + " al valor " + value + " en ReusableA2");
        }
    }    
}