/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

import es.uned.reusablelibrary.ReusableExtensibleDialog;
import es.uned.reusablelibrary.ReusableFormInterface;
import es.uned.reusablelibrary.exceptions.ChangeValueException;
import es.uned.reusablelibrary.exceptions.ReusableCleanException;
import es.uned.reusablelibrary.exceptions.ReusableSaveException;
import es.uned.reusablelibrary.exceptions.ReusableValidateException;
import es.uned.reusablelibrary.observer.ValueListener;
import java.util.Iterator;
import java.util.List;

/**
 * Diálogo ReusableC, que extiende la clase de diseñador GUI_C implementando los métodos de ReusableFormInterface.
 * 
 * <p><img src="doc-files/ProgrammerSwingDialog_class.png" alt="Diálogo Swing del Programador"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableC extends es.uned.reusablelibrary.test.repository.GUI_C implements ReusableFormInterface
{    
    /**
     * Diálogo reusable extensible padre del actual.
     */
    private ReusableExtensibleDialog parent;
    
    /**
     * Clase C actual (POJO).
     * Se cargará con los valores del formulario cuando se pulse el botón ACEPTAR.
     */
    protected C classC = null;

    /**
     * Clase C cargada (POJO).
     * Es la clase de inicialización, se guarda aparte para poder resetear a sus valores.
     */
    protected C loadedClassC = null;
    
    /**
     * Cuenta el número de instancias de la clase, para identificarlas individualmente a efectos del patrón Observer.
     */
    private static int numInstances = 0;
    
    /**
     * Identificador de la instancia en particular.
     */
    private int instanceId = 0;

    /**
     * Constructor por defecto.
     */
    public ReusableC() {
        super();
        super.init();
        ReusableC.numInstances++;
        this.instanceId = ReusableC.numInstances;
    }

    /**
     * Constructor.
     * 
     * @param parent ReusableExtensibleDialog Diálogo reusable extensible padre del actual.
     */
    public ReusableC(ReusableExtensibleDialog parent) {
        super();
        super.init();
        this.parent = parent;
        ReusableC.numInstances++;
        this.instanceId = ReusableC.numInstances;
    }

    /**
     * Inicializar diálogo.
     * 
     * @param classC C instancia para inicializar el formulario.
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario.
     */
    public Boolean setUpDialog(C classC) {
        // Inicializa claseC.
        initialize(classC);
            
        return true;
    }
    
     /**
     * Da de alta componentes para publicar sus valores mediante el patrón Observer y suscripciones a otros componentes para reproducir sus valores en los componentes indicados.
     * 
     * @param instances List lista de instancias a las que se le aplicará el patrón Observer para el paso de datos entre componentes.
     */
    public static void subscriptions(List<ReusableC> instances) {
       try {
            for (int i = 0, max = instances.size(); i < max; i++) {
                ReusableC instance = instances.get(i);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableC.campo6" + instance.instanceId, instance.campo6);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableC.campo7" + instance.instanceId, instance.campo7);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableC.campo8" + instance.instanceId, instance.campo8);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableC.campo9" + instance.instanceId, instance.campo9);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableC.campo10" + instance.instanceId, instance.campo10);
            }
            for (int i = 0, max = instances.size(); i < max; i++) {
                ReusableC instance = instances.get(i);
                for (int j = 1, max2 = instances.size(); j <= max2; j++) {
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableC.campo6" + j, new ValueListener(instance.campo6));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableC.campo7" + j, new ValueListener(instance.campo7));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableC.campo8" + j, new ValueListener(instance.campo8));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableC.campo9" + j, new ValueListener(instance.campo9));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableC.campo10" + j, new ValueListener(instance.campo10));
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Inicialización de la clase C.
     * 
     * @param classC C instancia para inicializar el formulario.
     */
    private void initialize(C classC) {
        if (classC == null) {
            this.loadedClassC = new C();
        }
        else {
            this.loadedClassC = classC;
        }
        this.classC = this.loadedClassC;
        
        super.campo6.setText(this.classC.getCampo6());
        super.campo7.setValue(this.classC.getCampo7());
        super.campo8.setText(this.classC.getCampo8());
        if (this.classC.getCampo9() != null) {
            String[] elements = this.classC.getCampo9().split(",");
            for (int i = 0, max = elements.length; i < max; i++) {
                super.campo9.setSelectedValue(elements[i], false);
            }
        }
        if ("OK".equals(this.classC.getCampo9()))
            super.campo10.setSelected(true);
    }

    /**
     * Obtener campo 6.
     * 
     * @return String Campo 6.
     */
    private String getCampo6() {
        return super.campo6.getText();
    }

    /**
     * Obtener campo 7.
     * 
     * @return String Campo 7.
     */
    private int getCampo7() {
        return super.campo7.getValue();
    }
    
    /**
     * Obtener campo 8.
     * 
     * @return String Campo 8.
     */
    private String getCampo8() {
        return super.campo8.getText();
    }
    
    /**
     * Obtener campo 9.
     * 
     * @return String Campo 9.
     */
    private String getCampo9() {
        List <String> elements = super.campo9.getSelectedValuesList();
        String result = "";
        Iterator<String> elts = elements.iterator();
        while(elts.hasNext()) {
            String element = elts.next();
            result += element + ",";
        }
        if (result.length() > 0) {
            result = result.substring(0, result.length()-1);
        }
        return result;
    }
    
    /**
     * Obtener campo 10.
     * 
     * @return String Campo 10.
     */
    private String getCampo10() {
        return super.campo10.isSelected() ? "OK" : "NO";
    }
    
    /**
     * Validacion de los campos del diálogo, si devuelve verdadero se ejecutan los otros dos métodos, si no, se continúa la edición.
     * 
     * @return TRUE si la validación es correcta, FALSE en caso contrario.
     * @throws ReusableValidateException si hubo algún error.
     */
    @Override
    public Boolean validateThis() throws ReusableValidateException {
        String log = "ReusableC.validateThis()\n";
        log +=       "========================\n";
        
        try {
            String lcampo6 = getCampo6();
            String lcampo8 = getCampo8();
            String lcampo9 = getCampo9();
            String lcampo10 = getCampo10();

            if (lcampo6 == null || lcampo6.trim().length() == 0) {
                throw new ReusableValidateException("Clase C.Campo 6 no puede ser vacío");            
            }
            if (lcampo8 == null || lcampo8.trim().length() == 0) {
                throw new ReusableValidateException("Clase C.Campo 8 no puede ser vacío");
            }
            if (lcampo9 == null || lcampo9.trim().length() == 0) {
                throw new ReusableValidateException("Clase C.Debe seleccionar algún valor en Campo 9");            
            }
            if (!"OK".equals(lcampo10)) {
                throw new ReusableValidateException("Clase C.Debe marcar el checkbox de Campo 10");            
            }
        }
        catch (ReusableValidateException e) {
            log += e.getLocalizedMessage() + "\n";
            System.out.println(log);
            throw new ReusableValidateException(e.getLocalizedMessage());
        }
        return true;
    }

    /**
     * Actualiza los campos de la clase editada desde los del diálogo.
     * 
     * @throws ReusableSaveException si hubo algún error.
     */
    @Override
    public void saveThis() throws ReusableSaveException {
        System.out.println("ReusableC.saveThis()");
        System.out.println("====================");
        try {
            classC.setCampo6(getCampo6());
            classC.setCampo7(getCampo7());
            classC.setCampo8(getCampo8());
            classC.setCampo9(getCampo9());
            classC.setCampo10(getCampo10());
            System.out.println(this.classC);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableSaveException(e.getLocalizedMessage());
        }
    }

    /**
     * Realiza tareas de finalización, cierre de ficheros, etc.
     * 
     * @throws ReusableCleanException si hubo algún error.
     */
    @Override
    public void cleanThis() throws ReusableCleanException {
        try {
            initialize(this.loadedClassC);
            System.out.println("ReusableC.cleanThis()");
            System.out.println("=====================");
            System.out.println(this.classC);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableCleanException(e.getLocalizedMessage());
        }
    }

    /**
     * Devuelve el contenido de la clase C con los valores actuales.
     * 
     * @return C la clase C con los valores actuales.
     */
    public C getClassC() {
        return this.classC;
    }
    
    /**
     * Emite un mensaje con una cadena de identificación y un valor.
     * 
     * @param identification String cadena de identificación.
     * @param value Object valor.
     * @throws ChangeValueException si hubo algún error.
     */
    @Override
    public void changeValue(String identification, Object value) throws ChangeValueException {
        try {
            ReusableExtensibleDialog.getEventManager().notify(identification, value);
        }
        catch (Exception e) {
            throw new ChangeValueException("Error al notificar un cambio de valor en " + identification + " al valor " + value + " en ReusableC");
        }
    }    
}