/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

import es.uned.reusablelibrary.ReusableExtensibleDialog;
import es.uned.reusablelibrary.ReusableFormInterface;
import es.uned.reusablelibrary.exceptions.ChangeValueException;
import es.uned.reusablelibrary.exceptions.ReusableCleanException;
import es.uned.reusablelibrary.exceptions.ReusableSaveException;
import es.uned.reusablelibrary.exceptions.ReusableValidateException;
import es.uned.reusablelibrary.observer.ValueListener;
import java.util.List;

/**
 * Diálogo ReusableA1, que extiende la clase de diseñador GUI_A1 implementando los métodos de ReusableFormInterface.
 * 
 * <p><img src="doc-files/ProgrammerSwingDialog_class.png" alt="Diálogo Swing del Programador"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableA1 extends es.uned.reusablelibrary.test.repository.GUI_A1 implements ReusableFormInterface
{    
    /**
     * Diálogo reusable extensible padre del actual.
     */
    private ReusableExtensibleDialog parent;
    
    /**
     * Clase A1 actual (POJO).
     * Se cargará con los valores del formulario cuando se pulse el botón ACEPTAR.
     */
    protected A1 classA1 = null;

    /**
     * Clase A1 cargada (POJO).
     * Es la clase de inicialización, se guarda aparte para poder resetear a sus valores.
     */
    protected A1 loadedClassA1 = null;
    
    /**
     * Cuenta el número de instancias de la clase, para identificarlas individualmente a efectos del patrón Observer.
     */
    private static int numInstances = 0;
    
    /**
     * Identificador de la instancia en particular.
     */
    private int instanceId = 0;
    
    /**
     * Constructor por defecto.
     */
    public ReusableA1() {
        super();
        super.init();
        ReusableA1.numInstances++;
        this.instanceId = ReusableA1.numInstances;
    }

    /**
     * Constructor.
     * 
     * @param parent ReusableExtensibleDialog Diálogo reusable extensible padre del actual.
     */
    public ReusableA1(ReusableExtensibleDialog parent) {
        super();
        super.init();
        this.parent = parent;
        ReusableA1.numInstances++;
        this.instanceId = ReusableA1.numInstances;
    }

    /**
     * Inicializar diálogo.
     * 
     * @param classA1 A1 instancia para inicializar el formulario.
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario.
     */
    public Boolean setUpDialog(A1 classA1) {
            // Inicializa classA1.
            initialize(classA1);
            return true;
    }
    
    /**
     * Da de alta componentes para publicar sus valores mediante el patrón Observer y suscripciones a otros componentes para reproducir sus valores en los componentes indicados.
     * 
     * @param instances List lista de instancias a las que se le aplicará el patrón Observer para el paso de datos entre componentes.
     */
    public static void subscriptions(List<ReusableA1> instances) {
        try {
            for (int i = 0, max = instances.size(); i < max; i++) {
                ReusableA1 instance = instances.get(i);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA1.jDial" + instance.instanceId, instance.jDial);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA1.velocidad" + instance.instanceId, instance.velocidad);
                ReusableExtensibleDialog.getEventManager().addOperation("ReusableA1.calidad" + instance.instanceId, instance.calidad);
            }
            for (int i = 0, max = instances.size(); i < max; i++) {
                ReusableA1 instance = instances.get(i);
                for (int j = 1, max2 = instances.size(); j <= max2; j++) {
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA1.jDial" + j, new ValueListener(instance.velocidad));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA1.velocidad" + j, new ValueListener(instance.jDial));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA1.calidad" + j, new ValueListener(instance.calidad));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA1.jDial" + j, new ValueListener(instance.jDial));
                    ReusableExtensibleDialog.getEventManager().subscribe("ReusableA1.velocidad" + j, new ValueListener(instance.velocidad));
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }
    
    /**
     * Inicialización de la clase A1.
     * 
     * @param classA1 A1 instancia para inicializar el formulario.
     */
    private void initialize(A1 classA1) {
        if (classA1 == null) {
            this.loadedClassA1 = new A1();
        }
        else {
            this.loadedClassA1 = classA1;
        }
        this.classA1 = this.loadedClassA1;

        super.velocidad.setValue(this.classA1.getVelocidad());
        super.jDial.setValue(this.classA1.getVelocidad());
        
        if (this.classA1.getCalidad() != null)
            super.calidad.setSelectedItem(this.classA1.getCalidad());
    }

    /**
     * Obtener velocidad.
     * 
     * @return int velocidad.
     */
    private int getVelocidad() {
        return super.velocidad.getValue();
    }

    /**
     * Obtener calidad.
     * 
     * @return String calidad.
     */
    private String getCalidad() {
        return super.calidad.getSelectedItem().toString();
    }
        
    /**
     * Validacion de los campos del diálogo, si devuelve verdadero se ejecutan los otros dos métodos, si no, se continúa la edición.
     * 
     * @return TRUE si la validación es correcta, FALSE en caso contrario.
     * @throws ReusableValidateException si hubo algún error.
     */
    @Override
    public Boolean validateThis() throws ReusableValidateException {
        String log = "ReusableA1.validateThis()\n";
        log +=       "=========================\n";
        String lcalidad = getCalidad();
        try {
            if (lcalidad == null || lcalidad.trim().length() == 0) {
                super.calidad.requestFocus();
                throw new ReusableValidateException("Clase A1.Campo Calidad no puede estar vacío");            
            }
        }
        catch (ReusableValidateException e) {
            log += e.getLocalizedMessage() + "\n";
            System.out.println(log);
            throw new ReusableValidateException(e.getLocalizedMessage());
           
        }
        return true;
    }
    
    /**
     * Actualiza los campos de la clase editada desde los del diálogo.
     * 
     * @throws ReusableSaveException si hubo algún error.
     */
    @Override
    public void saveThis() throws ReusableSaveException {
        System.out.println("ReusableA1.saveThis()");
        System.out.println("=====================");
        try {
            this.classA1.setVelocidad(getVelocidad());
            this.classA1.setCalidad(getCalidad());
            System.out.println(this.classA1);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableSaveException(e.getLocalizedMessage());
        }
    }

    /**
     * Realiza tareas de finalización, cierre de ficheros, etc.
     * 
     * @throws ReusableCleanException si hubo algún error.
     */
    @Override
    public void cleanThis() throws ReusableCleanException {
        try {
            initialize(this.loadedClassA1);
            System.out.println("ReusableA1.cleanThis()");
            System.out.println("=====================");
            System.out.println(this.classA1);
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new ReusableCleanException(e.getLocalizedMessage());
        }
    }

    /**
     * Devuelve el contenido de la clase A1 con los valores actuales.
     * 
     * @return A1 la clase A1 con los valores actuales.
     */
    public A1 getClassA1() {
        return this.classA1;
    }
    
    /**
     * Emite un mensaje con una cadena de identificación y un valor.
     * 
     * @param identification String cadena de identificación.
     * @param value Object valor.
     * @throws ChangeValueException si hubo algún error.
     */
    @Override
    public void changeValue(String identification, Object value) throws ChangeValueException {
        try {
            ReusableExtensibleDialog.getEventManager().notify(identification, value);
        }
        catch (Exception e) {
            throw new ChangeValueException("Error al notificar un cambio de valor en " + identification + " al valor " + value + " en ReusableA1");
        }
    }    
}