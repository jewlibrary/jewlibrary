/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under terms of GNU General Public License as published by
 * Free Software Foundation, either version 3 of License, or
 * (at your option) any later version.
 *
 * This program is distributed in hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

import java.util.Date;

/**
 * Clase B de ejemplo, contiene la información del diálogo GUI_B en una clase POJO (Plain Old Java Object), que es una clase básica independiente que no pertenece a ningún framework.
 * 
 * <p><img src="doc-files/POJO_class.png" alt="POJO class"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class B {

    /**
     * Constructor por defecto.
     */
    public B() {
        super();
    }
    
    /**
     * Inicializa la clase con una clase de tipo A.
     * 
     * @param classA A clase con los valores iniciales.
     */
    public void init(A classA) {
        this.datosGenerales = new A();
        this.datosGenerales.init(classA);
    }
    
    /**
     * Inicializa la clase con una clase de tipo A1.
     * 
     * @param classA1 A1 clase con los valores iniciales.
     */
    public void init(A1 classA1) {
        this.velocidad = new A1();
        this.velocidad.init(classA1);
    }
    
    /**
     * Inicializa la clase con una clase de tipo A2.
     * 
     * @param classA2 A2 clase con los valores iniciales.
     */
    public void init (A2 classA2) {
        this.ubicacion = new A2();
        this.ubicacion.init(classA2);
    }

    /**
     * Inicializa la clase con una clase de tipo C.
     * 
     * @param classC C clase con los valores iniciales.
     */
    public void init (C classC) {
        this.adicional = new C();
        this.adicional.init(classC);
    }

    /**
     * Inicializa la clase con una clase de tipo B.
     * 
     * @param classB B clase con los valores iniciales.
     */
    public void init (B classB) {
        this.setDatosGenerales(classB.getDatosGenerales());
        this.setVelocidad(classB.getVelocidad());
        this.setUbicacion(classB.getUbicacion());
        this.setCampo1(classB.getCampo1());
        this.setCampo2(classB.getCampo2());
        this.setCampo3(classB.getCampo3());
        this.setCampo4(classB.getCampo4());
        this.setCampo5(classB.getCampo5());
    }

    /**
     * Obtener Campo 1.
     * 
     * @return campo1 String campo 1.
     */
    public String getCampo1() {
        return campo1;
    }

    /**
     * Establecer campo 1.
     * 
     * @param campo1 String campo 1.
     */
    public void setCampo1(String campo1) {
        this.campo1 = campo1;
    }

    /**
     * Obtener Campo 2.
     * 
     * @return campo2 String campo 2.
     */
    public int getCampo2() {
        return campo2;
    }

    /**
     * Establecer campo 2.
     * 
     * @param campo2 String campo 2.
     */
    public void setCampo2(int campo2) {
        this.campo2 = campo2;
    }

    /**
     * Obtener Campo 3.
     * 
     * @return campo3 String campo 3.
     */
    public String getCampo3() {
        return campo3;
    }

    /**
     * Establecer campo 3.
     * 
     * @param campo3 String campo 3.
     */
    public void setCampo3(String campo3) {
        this.campo3 = campo3;
    }

    /**
     * Obtener Campo 4.
     * 
     * @return campo4 String campo 4.
     */
    public String getCampo4() {
        return campo4;
    }

    /**
     * Establecer campo 4.
     * 
     * @param campo4 String campo 4.
     */
    public void setCampo4(String campo4) {
        this.campo4 = campo4;
    }

    /**
     * Obtener Campo 5.
     * 
     * @return campo5 String campo 5.
     */
    public Date getCampo5() {
        return campo5;
    }

    /**
     * Establecer campo 5.
     * 
     * @param campo5 String campo 5.
     */
    public void setCampo5(Date campo5) {
        this.campo5 = campo5;
    }

    /**
     * Obtener datos generales.
     * 
     * @return datosGenerales A datos generales.
     */
    public A getDatosGenerales() {
        return datosGenerales;
    }

    /**
     * Establecer datos generales.
     * 
     * @param datosGenerales A datos generales.
     */
    public void setDatosGenerales(A datosGenerales) {
        this.datosGenerales = datosGenerales;
    }

    /**
     * Obtener velocidad.
     * 
     * @return velocidad A1 velocidad.
     */
    public A1 getVelocidad() {
        return velocidad;
    }

    /**
     * Establecer velocidad.
     * 
     * @param velocidad A1 velocidad.
     */
    public void setVelocidad(A1 velocidad) {
        this.velocidad = velocidad;
    }

    /**
     * Obtener ubicación.
     * 
     * @return ubicacion A2 ubicación.
     */
    public A2 getUbicacion() {
        return ubicacion;
    }

    /**
     * @param ubicacion ubicacion.
     */
    public void setUbicacion(A2 ubicacion) {
        this.ubicacion = ubicacion;
    }
    
    /**
     * Datos generales.
     */
    protected A datosGenerales;
    
    /**
     * Velocidad.
     */
    protected A1 velocidad;
    
    /**
     * Ubicación.
     */
    protected A2 ubicacion;
    
    /**
     * Adicional.
     */
    protected C adicional;
    
    /**
     * Campo 1.
     */
    protected String campo1 ="";
    
    /**
     * Campo 2.
     */
    protected int campo2 = 50;
    
    /**
     * Campo 3.
     */
    protected String campo3 = "";
    
    /**
     * Campo 4.
     */
    protected String campo4 = "";
    
    /**
     * Campo 5.
     */
    protected Date campo5 = new Date();
    
    /**
     * Devuelve la representación en formato de cadena de los valores de la clase.
     * 
     * @return String contenido de la clase.
     */
    public String toString() {
        String ret = "CLASE B:\n--------\n";
        /*
        ret += "datosGenerales =\n" + datosGenerales + "\n";
        ret += "velocidad = \n" + velocidad + "\n";
        ret += "ubicacion = \n" + ubicacion + "\n";
        ret += "adicional = \n" + adicional + "\n";
        */
        ret += "campo1 = " + campo1 + "\n";
        ret += "campo2 = " + campo2 + "\n";
        ret += "campo3 = " + campo3 + "\n";
        ret += "campo4 = " + campo4 + "\n";
        ret += "campo5 = " + campo5 + "\n";
        return ret;
    }    
}