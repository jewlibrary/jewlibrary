/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under terms of GNU General Public License as published by
 * Free Software Foundation, either version 3 of License, or
 * (at your option) any later version.
 *
 * This program is distributed in hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

/**
 * Clase A2 de ejemplo, contiene la información del diálogo GUI_A2 en una clase POJO (Plain Old Java Object), que es una clase básica independiente que no pertenece a ningún framework.
 * 
 * <p><img src="doc-files/POJO_class.png" alt="POJO class"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class A2 extends A {

    /**
     * Constructor por defecto.
     */
    public A2() {
        super();
    }
    
    /**
     * Inicializa la clase con una clase de tipo A.
     * 
     * @param classA A clase con los valores iniciales.
     */
    public void init(A classA) {
        super.init(classA);
    }

    /**
     * Inicializa la clase con una clase de tipo A2.
     * 
     * @param classA2 A2 clase con los valores iniciales.
     */
    public void init(A2 classA2) {
        this.setNombre(classA2.getNombre());
        this.setApellidos(classA2.getApellidos());
        this.setDni(classA2.getDni());
        this.setSexo(classA2.getSexo());
        this.setEstadoCivil(classA2.getEstadoCivil());
        this.setDireccion(classA2.getDireccion());
        this.setNum(classA2.getNum());
        this.setPiso(classA2.getPiso());
        this.setPuerta(classA2.getPuerta());
        this.setCodigoPostal(classA2.getCodigoPostal());
        this.setLocalidad(classA2.getLocalidad());
        this.setProvincia(classA2.getProvincia());
    }
    
    /**
     * Obtener la dirección.
     * 
     * @return direccion String dirección.
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Establecer la dirección.
     * 
     * @param direccion String dirección.
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Obtener el número.
     * 
     * @return num String número.
     */
    public String getNum() {
        return num;
    }

    /**
     * Establecer el número.
     * 
     * @param num String número.
     */
    public void setNum(String num) {
        this.num = num;
    }

    /**
     * Obtener el piso.
     * 
     * @return piso String piso.
     */
    public String getPiso() {
        return piso;
    }

    /**
     * Establecer el piso.
     * 
     * @param piso String piso.
     */
    public void setPiso(String piso) {
        this.piso = piso;
    }

    /**
     * Obtener la puerta.
     * 
     * @return puerta String puerta.
     */
    public String getPuerta() {
        return puerta;
    }

    /**
     * Establecer la puerta.
     * 
     * @param puerta String puerta.
     */
    public void setPuerta(String puerta) {
        this.puerta = puerta;
    }

    /**
     * Obtener el código postal.
     * 
     * @return codigoPostal String código postal.
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * Establecer el código postal.
     * 
     * @param codigoPostal String código postal.
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * Obtener la localidad.
     * 
     * @return localidad String localidad.
     */
    public String getLocalidad() {
        return localidad;
    }

    /**
     * Establecer la localidad.
     * 
     * @param localidad String localidad.
     */
    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    /**
     * Obtener la provincia.
     * 
     * @return provincia String provincia.
     */
    public String getProvincia() {
        return provincia;
    }

    /**
     * Establecer la provincia.
     * 
     * @param provincia String provincia.
     */
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    /**
     * Dirección.
     */
    protected String direccion = "";
    
    /**
     * Número.
     */
    protected String num = "";
    
    /**
     * Piso.
     */
    protected String piso = "";
    
    /**
     * Puerta.
     */
    protected String puerta = "";
    
    /**
     * Código postal.
     */
    protected String codigoPostal = "";
    
    /**
     * Localidad.
     */
    protected String localidad = "";
    
    /**
     * Provincia.
     */
    protected String provincia = "";

    /**
     * Devuelve la representación en formato de cadena de los valores de la clase.
     * 
     * @return String contenido de la clase.
     */
    public String toString() {
        String ret = "CLASE A2:\n--------\n";
        ret += "direccion = " + direccion + "\n";
        ret += "num = " + num + "\n";
        ret += "piso = " + piso + "\n";
        ret += "puerta = " + puerta + "\n";
        ret += "codigoPostal = " + codigoPostal + "\n";
        ret += "localidad = " + localidad + "\n";
        ret += "provincia = " + provincia + "\n";
        return ret;
    }
}
