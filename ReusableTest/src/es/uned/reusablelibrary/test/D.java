/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under terms of GNU General Public License as published by
 * Free Software Foundation, either version 3 of License, or
 * (at your option) any later version.
 *
 * This program is distributed in hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

/**
 * Clase D de ejemplo, contiene la información del diálogo GUI_D en una clase POJO (Plain Old Java Object), que es una clase básica independiente que no pertenece a ningún framework.
 * 
 * <p><img src="doc-files/POJO_class.png" alt="POJO class"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class D {

    /**
     * Constructor por defecto.
     */
    public D() {
        super();
    }
    
    /**
     * Inicializa la clase con una clase de tipo D.
     * 
     * @param classD D clase con los valores iniciales.
     */
    public void init(D classD) {
        this.setCampo11(classD.getCampo11());
        this.setCampo12(classD.getCampo12());
        this.setCampo13(classD.getCampo13());
        this.setCampo14(classD.getCampo14());
        this.setCampo15(classD.getCampo15());
    }
    
    /**
     * Obtener Campo 11.
     * 
     * @return campo11 String campo 11.
     */
    public String getCampo11() {
        return campo11;
    }

    /**
     * Establecer campo 11.
     * 
     * @param campo11 String campo 11.
     */
    public void setCampo11(String campo11) {
        this.campo11 = campo11;
    }

    /**
     * Obtener Campo 12.
     * 
     * @return campo12 String campo 12.
     */
    public int getCampo12() {
        return campo12;
    }

    /**
     * Establecer campo 12.
     * 
     * @param campo12 int campo 12.
     */
    public void setCampo12(int campo12) {
        this.campo12 = campo12;
    }

    /**
     * Obtener Campo 13.
     * 
     * @return campo13 String campo 13.
     */
    public String getCampo13() {
        return campo13;
    }

    /**
     * Establecer campo 13.
     * 
     * @param campo13 String campo 13.
     */
    public void setCampo13(String campo13) {
        this.campo13 = campo13;
    }

    /**
     * Obtener Campo 14.
     * 
     * @return campo14 String campo 14.
     */
    public String getCampo14() {
        return campo14;
    }

    /**
     * Establecer campo 14.
     * 
     * @param campo14 String campo 14.
     */
    public void setCampo14(String campo14) {
        this.campo14 = campo14;
    }

    /**
     * Obtener Campo 15.
     * 
     * @return campo15 String campo 15.
     */
    public String getCampo15() {
        return campo15;
    }

    /**
     * Establecer campo 15.
     * 
     * @param campo15 String campo 15.
     */
    public void setCampo15(String campo15) {
        this.campo15 = campo15;
    }
    
    /**
     * Campo 11.
     */
    protected String campo11;

    /**
     * Campo 12.
     */
    protected int campo12;

    /**
     * Campo 13.
     */
    protected String campo13;

    /**
     * Campo 14.
     */
    protected String campo14;
    
    /**
     * Campo 15.
     */
    protected String campo15;
    
    /**
     * Devuelve la representación en formato de cadena de los valores de la clase.
     * 
     * @return String contenido de la clase.
     */
    public String toString() {
        String ret = "CLASE D:\n--------\n";
        ret += "campo11 = " + getCampo11() + "\n";
        ret += "campo12 = " + getCampo12() + "\n";
        ret += "campo13 = " + getCampo13() + "\n";
        ret += "campo14 = " + getCampo14() + "\n";
        ret += "campo15 = " + getCampo15() + "\n";
        return ret;
    }    
}