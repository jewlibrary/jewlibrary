/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under terms of GNU General Public License as published by
 * Free Software Foundation, either version 3 of License, or
 * (at your option) any later version.
 *
 * This program is distributed in hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

/**
 * Clase C de ejemplo, contiene la información del diálogo GUI_C en una clase POJO (Plain Old Java Object), que es una clase básica independiente que no pertenece a ningún framework.
 * 
 * <p><img src="doc-files/POJO_class.png" alt="POJO class"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class C {
    
    /**
     * Constructor por defecto
     */
    public C() {
        super();
    }

    /**
     * Inicializa la clase con una clase de tipo D.
     * 
     * @param classD D clase con los valores iniciales.
     */
    public void init(D classD) {
        this.elementoAdicional = new D();
        this.elementoAdicional.init(classD);
    }

    /**
     * Inicializa la clase con una clase de tipo C.
     * 
     * @param classC C clase con los valores iniciales.
     */
    public void init(C classC) {
        this.setCampo6(classC.getCampo6());
        this.setCampo7(classC.getCampo7());
        this.setCampo8(classC.getCampo8());
        this.setCampo9(classC.getCampo9());
        this.setCampo10(classC.getCampo10());
        this.setElementoAdicional(classC.getElementoAdicional());
    }

    /**
     * Obtener Campo 6.
     * 
     * @return campo6 String campo 6.
     */
    public String getCampo6() {
        return campo6;
    }

    /**
     * Establecer campo 6.
     * 
     * @param campo6 String campo 6.
     */
    public void setCampo6(String campo6) {
        this.campo6 = campo6;
    }

    /**
     * Obtener Campo 7.
     * 
     * @return campo7 String campo 7.
     */
    public int getCampo7() {
        return campo7;
    }

    /**
     * Establecer campo 7.
     * 
     * @param campo7 String campo 7.
     */
    public void setCampo7(int campo7) {
        this.campo7 = campo7;
    }

    /**
     * Obtener Campo 8.
     * 
     * @return campo8 String campo 8.
     */
    public String getCampo8() {
        return campo8;
    }

    /**
     * Establecer campo 8.
     * 
     * @param campo8 String campo 8.
     */
    public void setCampo8(String campo8) {
        this.campo8 = campo8;
    }

    /**
     * Obtener Campo 9.
     * 
     * @return campo9 String campo 9.
     */
    public String getCampo9() {
        return campo9;
    }

    /**
     * Establecer campo 9.
     * 
     * @param campo9 String campo 9.
     */
    public void setCampo9(String campo9) {
        this.campo9 = campo9;
    }

    /**
     * Obtener Campo 10.
     * 
     * @return campo10 String campo 10.
     */
    public String getCampo10() {
        return campo10;
    }

    /**
     * Establecer campo 10.
     * 
     * @param campo10 String campo 10.
     */
    public void setCampo10(String campo10) {
        this.campo10 = campo10;
    }
    
    /**
     * Elemento Adicional.
     */
    protected D elementoAdicional;
    
    /**
     * Campo 6.
     */
    protected String campo6 = "";
    
    /**
     * Campo 7.
     */
    protected int campo7 = 0;
    
    /**
     * Campo 8.
     */
    protected String campo8 = "";
    
    /**
     * Campo 9.
     */
    protected String campo9 = "";
    
    /**
     * Campo 10.
     */
    protected String campo10 = "NO";

    /**
     * Obtener el elemento adicional.
     * 
     * @return elementoAdicional D elemento adicional.
     */
    public D getElementoAdicional() {
        return elementoAdicional;
    }

    /**
     * Establecer el elemento adicional.
     * 
     * @param elementoAdicional D elemento adicional.
     */
    public void setElementoAdicional(D elementoAdicional) {
        this.elementoAdicional = elementoAdicional;
    }

    /**
     * Devuelve la representación en formato de cadena de los valores de la clase.
     * 
     * @return String contenido de la clase.
     */
    public String toString() {
        String ret = "CLASE C:\n--------\n";
        //ret += "elementoAdicional =\n" + elementoAdicional + "\n";
        ret += "campo6 = " + campo6 + "\n";
        ret += "campo7 = " + campo7 + "\n";
        ret += "campo8 = " + campo8 + "\n";
        ret += "campo9 = " + campo9 + "\n";
        ret += "campo10 = " + campo10 + "\n";
        return ret;
    }
}