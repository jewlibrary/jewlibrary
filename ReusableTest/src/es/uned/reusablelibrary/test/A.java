/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under terms of GNU General Public License as published by
 * Free Software Foundation, either version 3 of License, or
 * (at your option) any later version.
 *
 * This program is distributed in hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;

/**
 * Clase A de ejemplo, contiene la información del diálogo GUI_A en una clase POJO (Plain Old Java Object), que es una clase básica independiente que no pertenece a ningún framework.
 * 
 * <p><img src="doc-files/POJO_class.png" alt="POJO class"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class A {

    /**
     * Constructor por defecto.
     */
    public A() {
        super();
    }

    /**
     * Inicializa la clase con una clase de tipo A.
     * 
     * @param classA A clase con los valores iniciales.
     */
    public void init(A classA) {
        this.setNombre(classA.getNombre());
        this.setApellidos(classA.getApellidos());
        this.setDni(classA.getDni());
        this.setSexo(classA.getSexo());
        this.setEstadoCivil(classA.getEstadoCivil());
    }
    
    /**
     * Obtener el nombre.
     * 
     * @return String el nombre.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Establecer el nombre.
     * 
     * @param nombre String el nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Obtener los apellidos.
     * 
     * @return String apellidos.
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Establecer los apellidos.
     * 
     * @param apellidos String apellidos.
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Obtener el DNI.
     * 
     * @return dni String DNI.
     */
    public String getDni() {
        return dni;
    }

    /**
     * Establecer el DNI.
     * 
     * @param dni String DNI. 
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * Obtener el estado civil.
     * 
     * @return String estado civil.
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Establecer el estado civil.
     * 
     * @param estadoCivil String estado civil.
     */
    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    /**
     * Obtener el sexo.
     * 
     * @return String sexo.
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * Establecer el sexo.
     * 
     * @param sexo String sexo. 
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    /**
     * Nombre.
     */
    protected String nombre = "";
    
    /**
     * Apellidos.
     */
    protected String apellidos = "";
    
    /**
     * DNI.
     */
    protected String dni = "";
    
    /**
     * Estado civil.
     */
    protected String estadoCivil = null;
    
    /**
     * Sexo.
     */
    protected String sexo = null;
    
    /**
     * Devuelve la representación en formato de cadena de los valores de la clase.
     * 
     * @return String contenido de la clase.
     */
    public String toString() {
        String ret = "CLASE A:\n--------\n";
        ret += "nombre = " + nombre + "\n";
        ret += "apellidos = " + apellidos + "\n";
        ret += "dni = " + dni + "\n";
        ret += "estadoCivil = " + estadoCivil + "\n";
        ret += "sexo = " + sexo + "\n";
        return ret;
    }    
}