/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under terms of GNU General Public License as published by
 * Free Software Foundation, either version 3 of License, or
 * (at your option) any later version.
 *
 * This program is distributed in hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.test;


import es.uned.reusablelibrary.DialogResizer;
import es.uned.reusablelibrary.ReusableDialogFactory;
import es.uned.reusablelibrary.ReusableExtensibleDialog;
import es.uned.reusablelibrary.ReusableTreeViewDialog;
import java.awt.Dimension;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Clase JUnit 4 de Test para realizar las pruebas unitarias de la biblioteca.
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableLibraryTest {

    /**
     * Constructor por defecto.
     */
    public ReusableLibraryTest() {
    }

    /**
     * Test de la clase DialogResizer con un solo diálogo.
     */
    @Test
    public void dialogResizer1DialogTest() {
        try {
            A classA = new A();
            ReusableExtensibleDialog main_dialog = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A");
            boolean setupMain = main_dialog.setUpDialog();
            boolean setupA = false;
            ReusableA rA = null;
            assertTrue(setupMain);
            if (setupMain) {
                rA = new ReusableA(main_dialog);
                setupA = rA.setUpDialog(classA);
                assertTrue(setupA);
                main_dialog.append(rA, "clase A");
            }
            
            Dimension expectedPreferredDimension = rA.getPreferredSize();
            Dimension expectedMinimumDimension = rA.getMinimumSize();

            Dimension preferredDimension = DialogResizer.getPreferredSize(main_dialog);
            Dimension minimumDimension = DialogResizer.getMinimumSize(main_dialog);

            assertEquals(expectedPreferredDimension.toString(), preferredDimension.toString());
            assertEquals(expectedMinimumDimension.toString(), minimumDimension.toString());
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        
    }

    /**
     * Test de la clase DialogResizer con un ReusableSimpleDialog de dos diálogos en distribución horizontal.
     */
    @Test
    public void dialogResizer2DialogHTest() {
        try {
            A classA = new A();
            A1 classA1 = new A1();
            ReusableA rA = null;
            ReusableA1 rA1 = null;
            ReusableExtensibleDialog main_dialog = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_HORIZONTAL, null, "Test A");
            boolean setupMain = false;
            boolean setupA = false;
            boolean setupA1 = false;
            
            setupMain = main_dialog.setUpDialog();
            assertTrue(setupMain);
            if (setupMain) {
                rA = new ReusableA(main_dialog);
                setupA = rA.setUpDialog(classA);
                assertTrue(setupA);
                if (setupA) {
                    rA1 = new ReusableA1(main_dialog);
                    setupA1 = rA1.setUpDialog(classA1);
                    assertTrue(setupA1);
                    main_dialog.append(rA, "clase A");
                    main_dialog.append(rA1, "clase A1");
                }
            }
            
            
            Dimension expectedPreferredDimension = new Dimension();
            expectedPreferredDimension.setSize(rA.getPreferredSize().getWidth() + rA1.getPreferredSize().getWidth(), Math.max(rA.getPreferredSize().getHeight(), rA1.getPreferredSize().getHeight()));
            Dimension expectedMinimumDimension =  new Dimension();
            expectedMinimumDimension.setSize(rA.getMinimumSize().getWidth() + rA1.getMinimumSize().getWidth(), Math.max(rA.getMinimumSize().getHeight(), rA1.getMinimumSize().getHeight()));

            Dimension preferredDimension = DialogResizer.getPreferredSize(main_dialog);
            Dimension minimumDimension = DialogResizer.getMinimumSize(main_dialog);
                        
            assertEquals(expectedPreferredDimension.toString(), preferredDimension.toString());
            assertEquals(expectedMinimumDimension.toString(), minimumDimension.toString());
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        
    }

    /**
     * Test de la clase DialogResizer con un ReusableSimpleDialog de dos diálogos y distribución vertical.
     */
    @Test
    public void dialogResizer2DialogVTest() {
        try {
            A classA = new A();
            A1 classA1 = new A1();
            ReusableA rA = null;
            ReusableA1 rA1 = null;
            ReusableExtensibleDialog main_dialog = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A");
            boolean setupMain = false;
            boolean setupA = false;
            boolean setupA1 = false;
            
            setupMain = main_dialog.setUpDialog();
            assertTrue(setupMain);
            if (setupMain) {
                rA = new ReusableA(main_dialog);
                setupA = rA.setUpDialog(classA);
                assertTrue(setupA);
                if (setupA) {
                    rA1 = new ReusableA1(main_dialog);
                    setupA1 = rA1.setUpDialog(classA1);
                    assertTrue(setupA1);
                    main_dialog.append(rA, "clase A");
                    main_dialog.append(rA1, "clase A1");
                }
            }
            
            
            Dimension expectedPreferredDimension = new Dimension();
            expectedPreferredDimension.setSize(Math.max(rA.getPreferredSize().getWidth(), rA1.getPreferredSize().getWidth()), rA.getPreferredSize().getHeight() + rA1.getPreferredSize().getHeight());
            Dimension expectedMinimumDimension =  new Dimension();
            expectedMinimumDimension.setSize(Math.max(rA.getMinimumSize().getWidth(), rA1.getMinimumSize().getWidth()), rA.getMinimumSize().getHeight() + rA1.getMinimumSize().getHeight());

            Dimension preferredDimension = DialogResizer.getPreferredSize(main_dialog);
            Dimension minimumDimension = DialogResizer.getMinimumSize(main_dialog);
            
            assertEquals(expectedPreferredDimension.toString(), preferredDimension.toString());
            assertEquals(expectedMinimumDimension.toString(), minimumDimension.toString());
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        
    }

    /**
     * Test de la clase DialogResizer con un ReusableTabDialog de dos diálogos.
     */
    @Test
    public void dialogResizer2DialogTabTest() {
        try {
            A classA = new A();
            A1 classA1 = new A1();
            ReusableA rA = null;
            ReusableA1 rA1 = null;
            ReusableExtensibleDialog main_dialog = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_TAB, ReusableDialogFactory.LayoutType.NONE, null, "Test A");
            boolean setupMain = false;
            boolean setupA = false;
            boolean setupA1 = false;
            
            setupMain = main_dialog.setUpDialog();
            assertTrue(setupMain);
            if (setupMain) {
                rA = new ReusableA(main_dialog);
                setupA = rA.setUpDialog(classA);
                assertTrue(setupA);
                if (setupA) {
                    rA1 = new ReusableA1(main_dialog);
                    setupA1 = rA1.setUpDialog(classA1);
                    assertTrue(setupA1);
                    main_dialog.append(rA, "clase A");
                    main_dialog.append(rA1, "clase A1");
                }
            }
            
            Dimension expectedPreferredDimension = new Dimension();
            expectedPreferredDimension.setSize(Math.max(rA.getPreferredSize().getWidth(), rA1.getPreferredSize().getWidth()), Math.max(rA.getPreferredSize().getHeight(), rA1.getPreferredSize().getHeight()));

            Dimension expectedMinimumDimension =  new Dimension();
            expectedMinimumDimension.setSize(Math.max(rA.getMinimumSize().getWidth(), rA1.getMinimumSize().getWidth()) + 5, Math.max(rA.getMinimumSize().getHeight(), rA1.getMinimumSize().getHeight()) + 28);

            Dimension preferredDimension = DialogResizer.getPreferredSize(main_dialog);
            Dimension minimumDimension = DialogResizer.getMinimumSize(main_dialog);
            // Se le suman 5 de ancho y 28 de alto por las dimensiones del dibujo de la tab
            minimumDimension.setSize(minimumDimension.getWidth() + 5, minimumDimension.getHeight() + 28);
                        
            assertEquals(expectedPreferredDimension.toString(), preferredDimension.toString());
            System.out.println(expectedMinimumDimension.toString() + " =" + minimumDimension.toString());
            assertEquals(expectedMinimumDimension.toString(), minimumDimension.toString());
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test de la clase DialogResizer con un ReusableTreeViewDialog de cuatro diálogos, incluyendo dentro un diálogo ReusableSimpleDialog de dos diálogos vertical.
     */
    @Test
    public void dialogResizerTreeViewTest() {
        try {
            A classA = new A();
            A1 classA1 = new A1();
            A2 classA2 = new A2();
            B classB = new B();
            classB.init(classA);
            classB.init(classA1);
            classB.init(classA2);

            boolean setupMain = false;
            boolean setupA = false;
            boolean setupA1 = false;
            boolean setupA2 = false;
            boolean setupB = false;
            boolean setupDialog_A2 = false;
            boolean setupA_A2 = false;
            boolean setupA2_A2 = false;
           
            Dimension expectedPreferredDimension = new Dimension();
            Dimension expectedMinimumDimension =  new Dimension();
            
            ReusableExtensibleDialog main_dialog = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_TREEVIEW, ReusableDialogFactory.LayoutType.NONE, null, "Raíz del árbol", "Nombre del árbol");
            setupMain = main_dialog.setUpDialog();
            assertTrue(setupMain);
            if (setupMain) {
                ReusableA rA = new ReusableA(main_dialog);
                setupA = rA.setUpDialog(classA);
                assertTrue(setupA);
                ReusableA1 rA1 = new ReusableA1(main_dialog);
                setupA1 = rA1.setUpDialog(classA1);
                assertTrue(setupA1);
                ReusableA2 rA2 = new ReusableA2(main_dialog);
                setupA2 = rA2.setUpDialog(classA2);
                assertTrue(setupA2);
                ReusableB rB = new ReusableB(main_dialog);
                setupB = rB.setUpDialog(classB);
                assertTrue(setupB);

                ReusableExtensibleDialog dialog_A2 = ReusableDialogFactory.createDialog(ReusableDialogFactory.DialogType.CONT_SIMPLE, ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL, null, "Test A2");
                setupDialog_A2 = dialog_A2.setUpDialog();
                assertTrue(setupDialog_A2);
                if (setupDialog_A2) {
                    ReusableA rA_A2 = new ReusableA(dialog_A2);
                    setupA_A2 = rA_A2.setUpDialog(classA);
                    assertTrue(setupA_A2);
                    ReusableA2 rA2_A2 = new ReusableA2(dialog_A2);
                    setupA2_A2 = rA_A2.setUpDialog(classA2);
                    assertTrue(setupA2_A2);
                    dialog_A2.append(rA_A2, "clase A");
                    dialog_A2.append(rA2_A2, "clase A2");
                }
                
                ((ReusableTreeViewDialog)main_dialog).append(rA, "claseA");
                ((ReusableTreeViewDialog)main_dialog).append(rA1, "claseA1");
                ((ReusableTreeViewDialog)main_dialog).addExtensibleChild(dialog_A2, "claseA2");
                ((ReusableTreeViewDialog)main_dialog).append(rB, "claseB");
                
                
                expectedPreferredDimension.setSize(Math.max(rA.getPreferredSize().getWidth(), Math.max(rA1.getPreferredSize().getWidth(), Math.max(dialog_A2.getPreferredSize().getWidth(), rB.getPreferredSize().getWidth()))) + ((ReusableTreeViewDialog)main_dialog).getTreeWidth() + 12.0, Math.max(rA.getPreferredSize().getHeight(), Math.max(rA1.getPreferredSize().getHeight(), Math.max(dialog_A2.getPreferredSize().getHeight(), rB.getPreferredSize().getHeight()))));
                expectedMinimumDimension.setSize(Math.max(rA.getMinimumSize().getWidth(), Math.max(rA1.getMinimumSize().getWidth(), Math.max(dialog_A2.getMinimumSize().getWidth(), rB.getMinimumSize().getWidth()))) + ((ReusableTreeViewDialog)main_dialog).getTreeWidth() + 12.0, Math.max(rA.getMinimumSize().getHeight(), Math.max(rA1.getMinimumSize().getHeight(), Math.max(dialog_A2.getMinimumSize().getHeight(), rB.getMinimumSize().getHeight()))));
            }
            

            Dimension preferredDimension = DialogResizer.getPreferredSize(main_dialog);
            Dimension minimumDimension = DialogResizer.getMinimumSize(main_dialog);
                        
            assertEquals(expectedPreferredDimension.toString(), preferredDimension.toString());
            assertEquals(expectedMinimumDimension.toString(), minimumDimension.toString());
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}