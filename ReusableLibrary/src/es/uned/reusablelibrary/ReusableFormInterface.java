/*
 * Copyright (C) 2021 Marcos Martínez Poladura
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary;

import es.uned.reusablelibrary.exceptions.ChangeValueException;
import es.uned.reusablelibrary.exceptions.ReusableCleanException;
import es.uned.reusablelibrary.exceptions.ReusableSaveException;
import es.uned.reusablelibrary.exceptions.ReusableValidateException;
import java.awt.Dimension;

/**
 * Interfaz que implementa los métodos de validación, salvaguarda, limpieza y paso de valores en los GUIs generados por el editor.
 * 
 * <p><img src="doc-files/ReusableFormInterface.png" alt="ReusableFormInterface"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public interface ReusableFormInterface {
    /**
     * Validacion de los campos del diálogo, si devuelve verdadero se ejecutan los otros dos métodos, si no, se continúa la edición.
     * 
     * @return TRUE si la validación es correcta, FALSE en caso contrario.
     * @throws ReusableValidateException si hubo algún error.
     */
    public Boolean validateThis() throws ReusableValidateException;

    /**
     * Actualiza los campos de la clase editada desde los del diálogo.
     * 
     * @throws ReusableSaveException si hubo algún error.
     */
    public void saveThis() throws ReusableSaveException;

    /**
     * Realiza tareas de finalización, cierre de ficheros, etc.
     * 
     * @throws ReusableCleanException si hubo algún error.
     */
    public void cleanThis() throws ReusableCleanException;
    
    /**
     * Emite un mensaje con una cadena de identificación y un valor.
     * 
     * @param identification String cadena de identificación.
     * @param value Object valor.
     * @throws ChangeValueException si hubo algún error.
     */
    public void changeValue(String identification, Object value) throws ChangeValueException;
    
    /**
     * Obtiene las dimensiones preferidas del componente.
     * 
     * @return Dimension tamaño preferido.
     */
    public Dimension getPreferredSize();

    /**
     * Establece las dimensiones preferidas del componente.
     * 
     * @param preferredSize Dimension tamaño preferido.
     */
    public void setPreferredSize(Dimension preferredSize);

    /**
     * Obtiene las dimensiones mínimas del componente.
     * 
     * @return Dimension tamaño mínimo.
     */
    public Dimension getMinimumSize();

    /**
     * Establece las dimiensiones mínimas del componente.
     * 
     * @param minimumSize Dimension tamaño mínimo.
     */
    public void setMinimumSize(Dimension minimumSize);
}
