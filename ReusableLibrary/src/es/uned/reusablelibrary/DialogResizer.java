/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary;

import java.awt.Dimension;
import javax.swing.JFrame;

/**
 * Gestiona los cambios de tamaño de los diálogos.
 * 
 * Casos posibles: 
 * - ReusableSimpleDialog HORIZONTAL:
 *      anchoFrame = preferredSize.Width 1 + preferredSize.Width 2.
 *      altoFrame = Max (preferredSize.Height 1, preferredSize.Height 2).
 * - ReusableSimpleDialog VERTICAL:
 *      anchoFrame = Max (preferredSize.Width 1, preferredSize.Width 2).
 *      altoFrame = preferredSize.Height 1 + preferredSize.Height 2.
 * - ReusableTabDialog:
 *      anchoFrame = Max (preferredSize.Width 1 .. n).
 *      altoFrame = Max (preferredSize.Height 1 .. n).
 * - ReusableTreeViewDialog:
 *      anchoFrame = anchoPanelIzquierdo + anchoSeparador + Max (preferredSize.Width 1 .. n).
 *      altoFrame = Max (altoPanelIzquierdo, Max (preferredSize.Height 1 .. n).
 * 
 * <p><img src="doc-files/DialogResizer_class.png" alt="DialogResizer"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class DialogResizer {
   
    /**
     * Enumeración para el tipo de redimensionamiento de la ventana principal.
     * 
     * Hay 2 tipos posibles, FIXED_DIALOG -que no permite redimensión de la ventana- 
     * y FREE_DIALOG -sí permite redimensionar-.
     */
    public enum ResizerType {
        /** 
         * Mantiene el tamaño mínimo de los diálogos.
         */
        FIXED_DIALOG, 
        /** 
         * Diálogos que se pueden redimensionar.
         */
        FREE_DIALOG
    };

    /**
     * Obtiene el tamaño mínimo final contando con todos los diálogos anidados.
     * Nota: esta clase es pública para que se pueda testear con JUnit.
     * 
     * @param dialog ReusableExtensibleDialog el diálogo raiz.
     * @return Dimension las dimensiones mínimas máximas.
     */
    public static Dimension getMinimumSize(ReusableExtensibleDialog dialog) {
        Dimension dimension = new Dimension();
        if (dialog.components.isEmpty()) { // Caso base, no tiene más hijos
            return dialog.getMinimumSize();
        }
        else { // Caso recurrente, tiene hijos
            switch(dialog.getClass().getCanonicalName()) {
                case "es.uned.reusablelibrary.ReusableSimpleDialog":
                    if (((ReusableSimpleDialog)dialog).getLayoutType() == ReusableDialogFactory.LayoutType.BOX_LAYOUT_HORIZONTAL) {
                        double ancho = 0.0;
                        double alto = 0.0;

                        for (int i = 0, max = dialog.components.size(); i < max; i++) {
                            ReusableFormInterface component = dialog.components.get(i);
                            ancho += component.getMinimumSize().getWidth();
                            alto = Math.max(alto, dialog.getMinimumSize().getHeight());
                        }
                        
                        for (int i = 0, max = dialog.children.size(); i < max; i++) {
                            ReusableExtensibleDialog child = dialog.children.get(i);
                            Dimension dm = getMinimumSize(child);
                            ancho = Math.max(ancho, dm.getWidth());
                            alto = Math.max(ancho, dm.getHeight());
                        }
                        dimension.setSize(ancho, alto);
                    }
                    else if (((ReusableSimpleDialog)dialog).getLayoutType() == ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL) {
                        double ancho = 0.0;
                        double alto = 0.0;
                        for (int i = 0, max = dialog.components.size(); i < max; i++) {
                            ReusableFormInterface component = dialog.components.get(i);
                            ancho = Math.max(ancho, component.getMinimumSize().getWidth());
                            alto += component.getMinimumSize().getHeight();                            
                        }
                        
                        for (int i = 0, max = dialog.children.size(); i < max; i++) {
                            ReusableExtensibleDialog child = dialog.children.get(i);
                            Dimension dm = getMinimumSize(child);
                            ancho = Math.max(ancho, dm.getWidth());
                            alto = Math.max(ancho, dm.getHeight());
                        }
                        dimension.setSize(ancho, alto);
                    }
                    break;
                case "es.uned.reusablelibrary.ReusableTabDialog":
                    double ancho = 0.0;
                    double alto = 0.0;
                    for (int i = 0, max = dialog.components.size(); i < max; i++) {
                        ReusableFormInterface component = dialog.components.get(i);
                        ancho = Math.max(ancho, component.getMinimumSize().getWidth());
                        alto = Math.max(alto, component.getMinimumSize().getHeight());
                    }
                        
                    for (int i = 0, max = dialog.children.size(); i < max; i++) {
                        ReusableExtensibleDialog child = dialog.children.get(i);
                        Dimension dm = getMinimumSize(child);
                        ancho = Math.max(ancho, dm.getWidth());
                        alto = Math.max(ancho, dm.getHeight());
                    }
                    dimension.setSize(ancho, alto);
                    break;
                case "es.uned.reusablelibrary.ReusableTreeViewDialog":
                    double anchoT = 0.0;
                    double altoT = 0.0;
                    
                    for (int i = 0, max = dialog.components.size(); i < max; i++) {
                        ReusableFormInterface component = dialog.components.get(i);
                        anchoT = Math.max(anchoT, component.getMinimumSize().getWidth());
                        altoT = Math.max(altoT, component.getMinimumSize().getHeight());
                    }
                        
                    for (int i = 0, max = dialog.children.size(); i < max; i++) {
                        ReusableExtensibleDialog child = dialog.children.get(i);
                        Dimension dm = getMinimumSize(child);
                        anchoT = Math.max(anchoT, dm.getWidth());
                        altoT = Math.max(anchoT, dm.getHeight());
                    }
                    dimension.setSize(anchoT + ((ReusableTreeViewDialog)dialog).getTreeWidth() + 12.0, altoT);
                    break;
                default:
                    return dialog.getMinimumSize();
            }            
        }
        return dimension;
    }
    
    /**
     * Obtiene el tamaño preferido final contando con todos los diálogos anidados.
     * Nota: esta clase es pública para que se pueda testear con JUnit.
     * 
     * @param dialog ReusableExtensibleDialog el diálogo raiz.
     * @return Dimension las dimensiones preferidas máximas.
     */
    public static Dimension getPreferredSize (ReusableExtensibleDialog dialog) {
        Dimension dimension = new Dimension();
        if (dialog.components.isEmpty()) { // Caso base, no tiene más hijos.
            return dialog.getPreferredSize();
        }
        else { // Caso recurrente, tiene hijos.
            switch(dialog.getClass().getCanonicalName()) {
                case "es.uned.reusablelibrary.ReusableSimpleDialog":
                    if (((ReusableSimpleDialog)dialog).getLayoutType() == ReusableDialogFactory.LayoutType.BOX_LAYOUT_HORIZONTAL) {
                        double ancho = 0.0;
                        double alto = 0.0;

                        for (int i = 0, max = dialog.components.size(); i < max; i++) {
                            ReusableFormInterface component = dialog.components.get(i);
                            ancho += component.getPreferredSize().getWidth();
                            alto = Math.max(alto, dialog.getPreferredSize().getHeight());
                        }
                        
                        for (int i = 0, max = dialog.children.size(); i < max; i++) {
                            ReusableExtensibleDialog child = dialog.children.get(i);
                            Dimension dm = getPreferredSize(child);
                            ancho = Math.max(ancho, dm.getWidth());
                            alto = Math.max(ancho, dm.getHeight());
                        }
                        dimension.setSize(ancho, alto);
                    }
                    else if (((ReusableSimpleDialog)dialog).getLayoutType() == ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL) {
                        double ancho = 0.0;
                        double alto = 0.0;
                        for (int i = 0, max = dialog.components.size(); i < max; i++) {
                            ReusableFormInterface component = dialog.components.get(i);
                            ancho = Math.max(ancho, component.getPreferredSize().getWidth());
                            alto += component.getPreferredSize().getHeight();                            
                        }
                        
                        for (int i = 0, max = dialog.children.size(); i < max; i++) {
                            ReusableExtensibleDialog child = dialog.children.get(i);
                            Dimension dm = getPreferredSize(child);
                            ancho = Math.max(ancho, dm.getWidth());
                            alto = Math.max(ancho, dm.getHeight());
                        }
                        dimension.setSize(ancho, alto);
                    }
                    break;
                case "es.uned.reusablelibrary.ReusableTabDialog":
                    double ancho = 0.0;
                    double alto = 0.0;
                    for (int i = 0, max = dialog.components.size(); i < max; i++) {
                        ReusableFormInterface component = dialog.components.get(i);
                        ancho = Math.max(ancho, component.getPreferredSize().getWidth());
                        alto = Math.max(alto, component.getPreferredSize().getHeight());
                    }
                        
                    for (int i = 0, max = dialog.children.size(); i < max; i++) {
                        ReusableExtensibleDialog child = dialog.children.get(i);
                        Dimension dm = getPreferredSize(child);
                        ancho = Math.max(ancho, dm.getWidth());
                        alto = Math.max(ancho, dm.getHeight());
                    }
                    dimension.setSize(ancho, alto);
                    break;
                case "es.uned.reusablelibrary.ReusableTreeViewDialog":
                    double anchoT = 0.0;
                    double altoT = 0.0;
                    
                    for (int i = 0, max = dialog.components.size(); i < max; i++) {
                        ReusableFormInterface component = dialog.components.get(i);
                        anchoT = Math.max(anchoT, component.getPreferredSize().getWidth());
                        altoT = Math.max(altoT, component.getPreferredSize().getHeight());
                    }
                        
                    for (int i = 0, max = dialog.children.size(); i < max; i++) {
                        ReusableExtensibleDialog child = dialog.children.get(i);
                        Dimension dm = getPreferredSize(child);
                        anchoT = Math.max(anchoT, dm.getWidth());
                        altoT = Math.max(altoT, dm.getHeight());
                    }
                    dimension.setSize(anchoT + ((ReusableTreeViewDialog)dialog).getTreeWidth() + 12.0, altoT);
                    break;
                default:
                    return dialog.getPreferredSize();
            }            
        }
        return dimension;        
    }  
    
    /**
     * Actualiza el JFrame al tamaño correcto despues de una redimensión.
     * 
     * @param frame JFrame ventana principal.
     * @param dialog ReusableExtensibleDialog el diálogo que queremos adaptar.
     */
    public static void updateSize(JFrame frame, ReusableExtensibleDialog dialog) {
        double totalHeight, totalWidth;
        Dimension d = getMinimumSize(dialog);

        if (frame.getContentPane().getWidth() < d.getWidth())
            totalWidth = d.getWidth();
        else
            totalWidth = frame.getContentPane().getWidth();
        
        if (frame.getContentPane().getHeight() < d.getHeight())
            totalHeight = d.getHeight();
        else
            totalHeight = frame.getContentPane().getHeight();
        
        d.setSize(totalWidth, totalHeight);
        frame.getContentPane().setSize(d);
    }
    
    /**
     * Inicializa el tamaño del JFrame según los tamaños preferidos del sistema.
     * 
     * @param frame JFrame el frame que contiene la aplicación.
     * @param dialog ReusableExtensibleDialog el diálogo a partir del cual calcularemos el tamaño.
     */
    public static void setupDialog(JFrame frame, ReusableExtensibleDialog dialog) {
        // Actualiza las dimensiones.
        Dimension dP;
        Dimension dM;
        dP = getPreferredSize(dialog);
        dP.setSize(dP.getWidth(), dP.getHeight() + 60); // Se suma 60 para tener en cuenta los botones del pie.
        dM = getMinimumSize(dialog);
        dM.setSize(dM.getWidth(), dM.getHeight() + 60); // Se suma 60 para tener en cuenta los botones del pie.

        frame.getContentPane().setPreferredSize(dP);
        frame.getContentPane().setMinimumSize(dM);

        frame.pack();
        frame.setMinimumSize(frame.getSize());
        frame.addComponentListener(new DialogResizerComponentListener(frame, dialog));
    }    
}