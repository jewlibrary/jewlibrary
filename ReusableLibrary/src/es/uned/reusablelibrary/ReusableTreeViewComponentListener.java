/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JTree;

/**
 * Listener para bloquear el cambio de tamaño en el JTree cuando se pulsa sobre un nodo.
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableTreeViewComponentListener implements ComponentListener {

    /**
     * El árbol con el que estamos trabajando.
     */
    private JTree tree;
        
    /**
     * Constructor.
     * 
     * @param tree JTree el árbol asociado al listener.
     */
    public ReusableTreeViewComponentListener(JTree tree) {
        this.tree = tree;
        expandAllNodes(tree, 0, this.tree.getRowCount());
    }
    
    @Override
    public void componentResized(ComponentEvent e) {
        this.tree.setMinimumSize(this.tree.getPreferredSize());
        expandAllNodes(this.tree, 0, this.tree.getRowCount());
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }

    /**
     * Expande todos los nodos del arbol. Uso: expandAllNodes (tree, 0, tree.getRowCount());
     * 
     * @param tree JTree el árbol que queremos expandir.
     * @param startingIndex int el nodo inicial.
     * @param rowCount int número de nodos.
     */
    private void expandAllNodes(JTree tree, int startingIndex, int rowCount){
        for(int i=startingIndex;i<rowCount;++i){
            tree.expandRow(i);
        }

        if(tree.getRowCount()!=rowCount){
            expandAllNodes(tree, rowCount, tree.getRowCount());
        }
    }    
}
