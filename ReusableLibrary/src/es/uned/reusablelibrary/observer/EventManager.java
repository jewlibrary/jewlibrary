/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 * Clase que implementa el patrón Observer para implementar el mecanismo de comunicación entre los distintos componentes de los GUIs. Funciona como un gestor de notificaciones: permite que un componente se suscriba a los cambios de otro, cuando se producen recibe una notifiación para cambiar su valor.
 * 
 * <p><img src="doc-files/es.uned.reusablelibrary.observer.EventManager.png" alt="EventManager"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class EventManager {
    /**
     * Lista de eventos observadores.
     */
    Map<String, List<EventListener>> listeners = new HashMap<>();

    /**
     * Constructor.
     * 
     * @param operations String... lista de cadenas que identifican los eventos observadores.
     */
    public EventManager(String... operations) {
        for (String operation : operations) {
            this.listeners.put(operation, new ArrayList<>());
        }
    }

    /**
     * Añadir un evento.
     * 
     * @param operation String identificador del evento observador.
     * @param component JComponent el componente asociado.
     * @throws java.lang.Exception si hubo algún error.
     */
    public void addOperation (String operation, JComponent component) throws Exception {
        List<EventListener> users = listeners.get(operation);
        if (users == null) {
            this.listeners.put(operation, new ArrayList<>());
        }
        ChangeListener changeListener = new ChangeListener(operation, component);
        Component myComponent = new Component(component);
        myComponent.addChangeListener(changeListener);
    }

    /**
     * Suscribirse a un evento.
     * 
     * @param eventType String identificador del evento.
     * @param listener EventListener evento.
     */
    public void subscribe(String eventType, EventListener listener) {
        List<EventListener> users = listeners.get(eventType);
        if (users == null) {
            this.listeners.put(eventType, new ArrayList<>());
            users = listeners.get(eventType);
        }
        users.add(listener);
    }

    /**
     * Des-suscribirse a un evento.
     * 
     * @param eventType String identificador del evento.
     * @param listener EventListener evento.
     */
    public void unsubscribe(String eventType, EventListener listener) {
        List<EventListener> users = listeners.get(eventType);
        if (users != null)
            users.remove(listener);
    }

    /**
     * Notificar el evento.
     * 
     * @param eventType String identificador del evento.
     * @param value Object nuevo valor del elemento.
     * @throws java.lang.Exception si hubo algún error.
     */
    public void notify(String eventType, Object value) throws Exception {
        List<EventListener> users = listeners.get(eventType);
        for (EventListener listener : users) {
            listener.update(eventType, value);
        }
    }    
}