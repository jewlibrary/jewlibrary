/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.observer;

import javax.swing.JComponent;

/**
 * Cambia un valor al recibir una notificación.
 * 
 * <p><img src="doc-files/es.uned.reusablelibrary.observer.ValueListener.png" alt="ValueListener"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ValueListener implements EventListener {

    /**
     * Componente que se modifica al recibir una notificación.
     */
    private Component component;
    
    /**
     * Constructor
     * 
     * @param component JComponent el componente sobre el que se va a trabajar.
     */
    public ValueListener(JComponent component) {
        this.component = new Component(component);
    }

    /**
     * Devolver el componente sobre el que se trabaja.
     * 
     * @return int valor guardado.
     */
    public Component getComponent() {
        return this.component;
    }
    
    /**
     * Actualización del evento.
     * 
     * @param eventType String tipo de evento.
     * @param value Object nuevo valor del elemento.
     * @throws java.lang.Exception si hubo algún error.
     */
    @Override
    public void update(String eventType, Object value) 
        throws Exception {
        this.component.setValue(value);
    }
}