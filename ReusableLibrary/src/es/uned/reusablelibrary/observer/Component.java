/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.observer;

import java.awt.Color;
import javax.swing.AbstractButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolTip;
import javax.swing.JViewport;

/**
 * Envuelve a un componente de Swing para trabajar con él de manera única.
 * 
 * <p><img src="doc-files/es.uned.reusablelibrary.observer.Component.png" alt="Component"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class Component {
    
    /**
     * Componente swing con el que trataremos.
     */
    private JComponent component;
    
    /**
     * Constructor.
     * 
     * @param component JComponent componente con el que vamos a trabajar.
     */
    public Component (JComponent component) {
        this.component = component;
    }
    
    /**
     * Devuelve el valor del componente.
     * 
     * @return Object valor del componente.
     */
    public Object getValue() {
        Object value = null;
        if (JSpinner.class.isAssignableFrom(this.component.getClass())) {
            value = ((JSpinner)this.component).getValue();
        }
        else if (JSlider.class.isAssignableFrom(this.component.getClass())) {
            value = ((JSlider)this.component).getValue();
        }
        else if (JProgressBar.class.isAssignableFrom(this.component.getClass())) {
            value = ((JProgressBar)this.component).getValue();
        }
        else if (JLabel.class.isAssignableFrom(this.component.getClass())) {
            value = ((JLabel)this.component).getText();
        }
        else if (AbstractButton.class.isAssignableFrom(this.component.getClass())) {
            value = ((AbstractButton)this.component).getText();
        }
        else if (JColorChooser.class.isAssignableFrom(this.component.getClass())) {
            value = ((JColorChooser)this.component).getColor();
        }
        else if (JComboBox.class.isAssignableFrom(this.component.getClass())) {
            value = ((JComboBox)this.component).getSelectedItem();
        }
        else if (JFileChooser.class.isAssignableFrom(this.component.getClass())) {
            value = ((JFileChooser)this.component).getSelectedFile();
        }
        else if (JInternalFrame.class.isAssignableFrom(this.component.getClass())) {
            value = ((JInternalFrame)this.component).getTitle();
        }
        else if (JList.class.isAssignableFrom(this.component.getClass())) {
            value = ((JList)this.component).getSelectedIndex();
        }
        else if (JToolTip.class.isAssignableFrom(this.component.getClass())) {
            value = ((JToolTip)this.component).getTipText();
        }
        else if (JTextField.class.isAssignableFrom(this.component.getClass())) {
            value = ((JTextField)this.component).getText();
        }
        else if (JFormattedTextField.class.isAssignableFrom(this.component.getClass())) {
            value = ((JFormattedTextField)this.component).getText();
        }
        else if (JTextArea.class.isAssignableFrom(this.component.getClass())) {
            value = ((JTextArea)this.component).getText();
        }
        else {
            value = "";
        }
        
        return value;
    }
    
    /**
     * Establece el valor del objeto.
     * 
     * @param value Object el valor del objeto.
     */
    public void setValue(Object value) {
        try {
            if (JSpinner.class.isAssignableFrom(this.component.getClass())) {
                try {
                    // Probar si es un entero
                    int v = Integer.parseInt("" + value);
                    ((JSpinner)this.component).setValue(v);
                }
                catch (NumberFormatException e) {
                    // No es un entero, asignar el objeto completo
                    ((JSpinner)this.component).setValue(value);
                }
                
            }
            else if (JSlider.class.isAssignableFrom(this.component.getClass())) {
                ((JSlider)this.component).setValue(Integer.parseInt("" + value));
            }
            else if (JProgressBar.class.isAssignableFrom(this.component.getClass())) {
                ((JProgressBar)this.component).setValue(Integer.parseInt("" + value));
            }
            else if (JLabel.class.isAssignableFrom(this.component.getClass())) {
                ((JLabel)this.component).setText("" + value);
            }
            else if (AbstractButton.class.isAssignableFrom(this.component.getClass())) {
                ((AbstractButton)this.component).setSelected(true);
            }
            else if (JColorChooser.class.isAssignableFrom(this.component.getClass())) {
                ((JColorChooser)this.component).setColor((Color)value);
            }
            else if (JComboBox.class.isAssignableFrom(this.component.getClass())) {
                ((JComboBox)this.component).setSelectedItem(value);
            }
            else if (JInternalFrame.class.isAssignableFrom(this.component.getClass())) {
                ((JInternalFrame)this.component).setTitle("" + value);
            }
            else if (JList.class.isAssignableFrom(this.component.getClass())) {
                ((JList)this.component).setSelectedIndex((int)value);
            }
            else if (JToolTip.class.isAssignableFrom(this.component.getClass())) {
                ((JToolTip)this.component).setTipText("" + value);
            }
            else if (JTextField.class.isAssignableFrom(this.component.getClass())) {
                ((JTextField)this.component).setText("" + value);
            }
            else if (JFormattedTextField.class.isAssignableFrom(this.component.getClass())) {
                ((JFormattedTextField)this.component).setText("" + value);
            }
            else if (JTextArea.class.isAssignableFrom(this.component.getClass())) {
                ((JTextArea)this.component).setText("" + value);
            }
        }
        catch (NumberFormatException e) {
            System.err.println("Component.setValue: Error al asignar el valor " + value + " (" + value.getClass().getCanonicalName() + ") al componente de tipo " + this.component.getClass().getCanonicalName() + " = " + e.getMessage());
        }
    }
    
    /**
     * Añade el listener para gestionar los cambios en los componentes.
     * 
     * @param changeListener ChangeListener gestor de cambios del componente.
     */
    public void addChangeListener(ChangeListener changeListener) {
        if (JSlider.class.isAssignableFrom(this.component.getClass())) {
            ((JSlider) this.component).addChangeListener(changeListener);
        }
        else if (JSpinner.class.isAssignableFrom(this.component.getClass())) {
            ((JSpinner) this.component).addChangeListener(changeListener);
        }
        else if (AbstractButton.class.isAssignableFrom(this.component.getClass())) {
            ((AbstractButton) this.component).addItemListener(changeListener);
        }
        else if (JProgressBar.class.isAssignableFrom(this.component.getClass())) {
            ((JProgressBar) this.component).addChangeListener(changeListener);
        }
        else if (JTabbedPane.class.isAssignableFrom(this.component.getClass())) {
            ((JTabbedPane) this.component).addChangeListener(changeListener);
        }
        else if (JViewport.class.isAssignableFrom(this.component.getClass())) {
            ((JViewport) this.component).addChangeListener(changeListener);
        }
        else if (JList.class.isAssignableFrom(this.component.getClass())) {
            ((JList) this.component).addListSelectionListener(changeListener);
        }
        else if (JComboBox.class.isAssignableFrom(this.component.getClass())) {
            ((JComboBox) this.component).addItemListener(changeListener);
        }
        else if (JTextField.class.isAssignableFrom(this.component.getClass())) {
            ((JTextField) this.component).addKeyListener(changeListener);
        }
        else if (JFormattedTextField.class.isAssignableFrom(this.component.getClass())) {
            ((JFormattedTextField) this.component).addKeyListener(changeListener);
        }
        else if (JTextArea.class.isAssignableFrom(this.component.getClass())) {
            ((JTextArea) this.component).addKeyListener(changeListener);
        }
        else {
            this.component.addPropertyChangeListener(changeListener);
        }       
    }
}