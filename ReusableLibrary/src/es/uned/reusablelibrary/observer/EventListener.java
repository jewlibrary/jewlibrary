/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.observer;

/**
 * Intefaz observadora común.
 * 
 * <p><img src="doc-files/es.uned.reusablelibrary.observer.EventListener.png" alt="EventListener"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public interface EventListener {
    /**
     * Actualización del evento.
     * 
     * @param eventType String tipo de evento.
     * @param value Object nuevo valor del elemento.
     * @throws java.lang.Exception si hubo algún error.
     */
    void update(String eventType, Object value) throws Exception;
}