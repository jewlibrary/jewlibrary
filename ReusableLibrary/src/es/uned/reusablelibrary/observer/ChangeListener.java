/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.observer;

import es.uned.reusablelibrary.ReusableFormInterface;
import es.uned.reusablelibrary.exceptions.ChangeValueException;
import java.awt.Container;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import javax.swing.AbstractButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolTip;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * ChangeListener, controla los cambios en la propiedad text o value de un componente.
 * 
 * <p><img src="doc-files/es.uned.reusablelibrary.observer.ChangeListener.png" alt="ChangeListener"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ChangeListener 
        implements  javax.swing.event.ChangeListener, 
                    java.beans.PropertyChangeListener, 
                    KeyListener, 
                    ItemListener,
                    ListSelectionListener
                    {
    /**
     * Componente sobre el que se observan los cambios.
     */
    private JComponent component;
    
    /**
     * Tipo de evento.
     */
    private String operation;
    
    /**
     * Constructor.
     * 
     * @param operation String tipo de evento.
     * @param component JComponent componente sobre el que se observan los cambios.
     */
    public ChangeListener(String operation, JComponent component) {
        this.component = component;
        this.operation = operation;
    }
    
    /**
     * Devuelve el componente.
     * 
     * @return JComponent componente sobre el que se observan los cambios.
     */
    public JComponent getComponent() {
        return component;
    }

    /**
     * Se invoca cuando el destinatario del listener ha cambiado su estado.
     * 
     * @param e ChangeEvent evento producido por el objeto destinatario.
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        Object value = null;
        
        if (JSpinner.class.isAssignableFrom(this.component.getClass())) {
            value = ((JSpinner)this.component).getValue();
        }
        else if (JSlider.class.isAssignableFrom(this.component.getClass())) {
            value = ((JSlider)this.component).getValue();
        }
        else if (JProgressBar.class.isAssignableFrom(this.component.getClass())) {
            value = ((JProgressBar)this.component).getValue();
        }
        else if (JLabel.class.isAssignableFrom(this.component.getClass())) {
            value = ((JLabel)this.component).getText();
        }
        else if (AbstractButton.class.isAssignableFrom(this.component.getClass())) {
            value = ((AbstractButton)this.component).getText();
        }
        else if (JColorChooser.class.isAssignableFrom(this.component.getClass())) {
            value = ((JColorChooser)this.component).getColor();
        }
        else if (JComboBox.class.isAssignableFrom(this.component.getClass())) {
            value = ((JComboBox)this.component).getSelectedItem();
        }
        else if (JInternalFrame.class.isAssignableFrom(this.component.getClass())) {
            value = ((JInternalFrame)this.component).getTitle();
        }
        else if (JToolTip.class.isAssignableFrom(this.component.getClass())) {
            value = ((JToolTip)this.component).getTipText();
        }

        // Busca el componente de tipo ReusableFormInterface y llama al método cambiaVal con el valor recuperado.
        Container parent = component.getParent();
        while (parent != null) {
            if (ReusableFormInterface.class.isAssignableFrom(parent.getClass())) {
                try {
                    ((ReusableFormInterface)parent).changeValue(this.operation, value);
                }
                catch (ChangeValueException ec) {
                }
                break;
            }
            else {
                parent = parent.getParent();
            }
        } 
    }

    /**
     * Se invoca cuando cambia una propiedad del objeto asociado.
     * 
     * @param evt PropertyChangeEvent evento producido por el objeto destinatario.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (!"text".equals(evt.getPropertyName()))
            return;
        
        Object value = evt.getNewValue();

        // Busca el componente de tipo ReusableFormInterface y llama al
        // método cambiaVal con el valor recuperado
        Container parent = component.getParent();
        while (parent != null) {
            if (ReusableFormInterface.class.isAssignableFrom(parent.getClass())) {
                try {
                    ((ReusableFormInterface)parent).changeValue(this.operation, value);
                }
                catch (ChangeValueException ec) {
                }
                break;
            }
            else {
                parent = parent.getParent();
            }
        } 
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        String value = "";
        if (JTextField.class.isAssignableFrom(this.component.getClass())) {
            value = ((JTextField)this.component).getText();
        }
        else if (JTextArea.class.isAssignableFrom(this.component.getClass())) {
            value = ((JTextArea)this.component).getText();
        }
        else if (JFormattedTextField.class.isAssignableFrom(this.component.getClass())) {
            value = ((JFormattedTextField)this.component).getText();
        }

        // Busca el componente de tipo ReusableFormInterface y llama al método cambiaVal con el valor recuperado.
        Container parent = component.getParent();
        while (parent != null) {
            if (ReusableFormInterface.class.isAssignableFrom(parent.getClass())) {
                try {
                    ((ReusableFormInterface)parent).changeValue(this.operation, value);
                }
                catch (ChangeValueException ec) {
                }
                break;
            }
            else {
                parent = parent.getParent();
            }
        }         
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        Object value = "";
        if (e.getStateChange() == ItemEvent.SELECTED) {
            if (AbstractButton.class.isAssignableFrom(this.component.getClass())) {
                value = ((AbstractButton)this.component).getText();
            }
            else if (JComboBox.class.isAssignableFrom(this.component.getClass())) {
                value = ((JComboBox)this.component).getSelectedItem();
            }

            // Busca el componente de tipo ReusableFormInterface y llama al método cambiaVal con el valor recuperado.
            Container parent = component.getParent();
            while (parent != null) {
                if (ReusableFormInterface.class.isAssignableFrom(parent.getClass())) {
                    try {
                        ((ReusableFormInterface)parent).changeValue(this.operation, value);
                    }
                    catch (ChangeValueException ec) {
                    }
                    break;
                }
                else {
                    parent = parent.getParent();
                }
            }         
        
        }        
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object value = "";
        
        if (JList.class.isAssignableFrom(this.component.getClass())) {
            value = ((JList)this.component).getSelectedIndex();
        }

        // Busca el componente de tipo ReusableFormInterface y llama al método cambiaVal con el valor recuperado.
        Container parent = component.getParent();
        while (parent != null) {
            if (ReusableFormInterface.class.isAssignableFrom(parent.getClass())) {
                try {
                    ((ReusableFormInterface)parent).changeValue(this.operation, value);
                }
                catch (ChangeValueException ec) {
                }
                break;
            }
            else {
                parent = parent.getParent();
            }
        }
    }
}
