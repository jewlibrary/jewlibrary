/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary;

import es.uned.reusablelibrary.exceptions.ReusableFactoryException;

/**
 * Clase factoría para poder generar las interfaces reusables al vuelo desde el código.
 * 
 * <p><img src="doc-files/ReusableDialogFactory_class.png" alt="ReusableDialogFactory"></p>
 * 
 * @version 1.0
 *
 * @author Marcos Martínez Poladura
 */
public class ReusableDialogFactory {
    /**
     * Tipo de diálogo.
     * 
     * posibilidades: CONT_SIMPLE, CONT_TAB, CONT_TREEVIEW, CONT_TOOLBOX.
     * 
     * Nota: CONT_TOOLBOX no está implementada aún.
     */
    public enum DialogType {
        /** 
         * Tipo de diálogo simple.
         */
        CONT_SIMPLE, 
        /** 
         * Tipo de diálogo con pestañas.
         */
        CONT_TAB,         
        /** 
         * Tipo de diálogo con vista de árbol.
         */
        CONT_TREEVIEW, 
        /** 
         * Tipo de diálogo caja de herramientas (sin implementar en esta versión).
         */
        CONT_TOOLBOX
    };
    
    /**
     * Tipo de distribución (layout).
     * 
     * posibilidades: BOX_LAYOUT_HORIZONTAL, BOX_LAYOUT_VERTICAL, NONE.
     */
    public enum LayoutType {
        /**
         * distribución BoxLayout horizontal de Java.
         */
        BOX_LAYOUT_HORIZONTAL, 
        /**
         * distribución BoxLayout vertical de Java.
         */
        BOX_LAYOUT_VERTICAL, 
        /**
         * Ninguno.
         */
        NONE
    };

    /**
     * Crea un nuevo diálogo, con los botones de aceptar y cancelar.
     * 
     * @param contType DialogType tipo de contenedor a crear (CONT_SIMPLE, CONT_TAB o CONT_TREEVIEW).
     * @return ReusableExtensibleDialog diálogo creado.
     * @throws es.uned.reusablelibrary.exceptions.ReusableFactoryException Excepción si hay problemas al crear el diálogo.
     */
    public static ReusableExtensibleDialog createDialog(DialogType contType)
        throws ReusableFactoryException {
        return createDialog(contType, null, "");
    }

    /**
     * Crea un nuevo diálogo, con los botones de aceptar y cancelar.
     * 
     * @param contType DialogType tipo de contenedor a crear (CONT_SIMPLE, CONT_TAB o CONT_TREEVIEW).
     * @param name String nombre del diálogo.
     * @return ReusableExtensibleDialog diálogo creado.
     * @throws es.uned.reusablelibrary.exceptions.ReusableFactoryException Excepción si hay problemas al crear el diálogo.
     */
    public static ReusableExtensibleDialog createDialog(DialogType contType, String name)
        throws ReusableFactoryException {
        return createDialog(contType, null, name);
    }

    /**
     * Crea un nuevo diálogo, con los botones de aceptar y cancelar.
     * 
     * @param contType int tipo de contenedor a crear (CONT_SIMPLE, CONT_TAB o CONT_TREEVIEW).
     * @param parent ReusableExtensibleDialog diálogo padre que lo contiene.
     * @param name String nombre del contenedor.
     * @return JFrame diálogo creado.
     * @throws es.uned.reusablelibrary.exceptions.ReusableFactoryException Excepción si hay problemas al crear el diálogo.
     */
    public static ReusableExtensibleDialog createDialog(DialogType contType, ReusableExtensibleDialog parent, String name) 
        throws ReusableFactoryException {
        ReusableExtensibleDialog dialog = null;
        
        switch(contType) {
            case CONT_SIMPLE: 
                dialog = new ReusableSimpleDialog(parent, LayoutType.BOX_LAYOUT_VERTICAL, name);
                break;
            case CONT_TAB: 
                dialog = new ReusableTabDialog(parent, name);
                break;
            case CONT_TREEVIEW: 
                dialog = new ReusableTreeViewDialog(parent, name);
                break;
            default:
        }
        return dialog;
   }    

    /**
     * Crea un nuevo diálogo indicando el tipo de layout.
     * 
     * @param dialogType DialogType tipo de contenedor a crear (CONT_SIMPLE, CONT_TAB o CONT_TREEVIEW).
     * @param layoutType LayoutType tipo de layout del contenedor principal.
     * @param parent ReusableExtensibleDialog diálogo padre que lo contiene.
     * @param name String nombre del contenedor.
     * @return JFrame diálogo creado.
     * @throws es.uned.reusablelibrary.exceptions.ReusableFactoryException Excepción si hay problemas al crear el diálogo.
     */
    public static ReusableExtensibleDialog createDialog(DialogType dialogType, LayoutType layoutType, ReusableExtensibleDialog parent, String name) 
        throws ReusableFactoryException {
        ReusableExtensibleDialog dialog = null;
        
        switch(dialogType) {
            case CONT_SIMPLE: 
                dialog = new ReusableSimpleDialog(parent, layoutType, name);
                break;
            case CONT_TAB: 
                dialog = new ReusableTabDialog(parent, name);
                break;
            case CONT_TREEVIEW: 
                dialog = new ReusableTreeViewDialog(parent, name);
                break;
            default:
        }
        return dialog;
   }    

    /**
     * Crea un nuevo diálogo indicando el tipo de layout.
     * 
     * @param dialogType DialogType tipo de contenedor a crear (CONT_SIMPLE, CONT_TAB o CONT_TREEVIEW).
     * @param layoutType LayoutType tipo de layout del contenedor principal.
     * @param parent ReusableExtensibleDialog diálogo padre que lo contiene.
     * @param name String nombre del contenedor.
     * @param navigatorName String nombre del árbol.
     * @return JFrame diálogo creado.
     * @throws es.uned.reusablelibrary.exceptions.ReusableFactoryException Excepción si hay problemas al crear el diálogo.
     */
    public static ReusableExtensibleDialog createDialog(DialogType dialogType, LayoutType layoutType, ReusableExtensibleDialog parent, String name, String navigatorName) 
        throws ReusableFactoryException {
        ReusableExtensibleDialog dialog = null;
        
        switch(dialogType) {
            case CONT_SIMPLE: 
                dialog = new ReusableSimpleDialog(parent, layoutType, name);
                break;
            case CONT_TAB: 
                dialog = new ReusableTabDialog(parent, name);
                break;
            case CONT_TREEVIEW: 
                dialog = new ReusableTreeViewDialog(parent, name, navigatorName);
                break;
            default:
        }
        return dialog;
   }    
}