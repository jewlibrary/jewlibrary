/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary;

import java.awt.Container;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


/**
 * Clase para la validación del diálogo, que extiende ActionListener, y que se lanza con el botón ACEPTAR del diálogo principal.
 * 
 * <p><img src="doc-files/ValidationListener_class.png" alt="ValidationListener"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ValidationListener implements java.awt.event.ActionListener {

    /**
     * Clase sobre la que aplica.
     */
    private ReusableExtensibleDialog source;
    
    /**
     * Constructor.
     * 
     * @param source ReusableExtensibleDialog objeto sobre el que se aplica.
     */
    public ValidationListener(ReusableExtensibleDialog source) {
        this.source = source;
    }
    
    @Override
    public void actionPerformed(ActionEvent evt) {
        if (this.source.validateAll()) {
            this.source.saveAll();
            this.source.cleanAll();

            // Si está todo bien, cierra la ventana
            Container c = (Container) evt.getSource();

            while (!"javax.swing.JFrame".equals(c.getClass().getCanonicalName())) {
                c = c.getParent();
            }
            JOptionPane.showMessageDialog(c, "Diálogo validado correctamente");
            
            // CODIGO COMENTADO, PARA CERRAR LA VENTANA.
            //((JFrame)c).dispose();
        }
    }    
}