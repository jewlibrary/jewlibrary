/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary;

import java.awt.Container;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Nodo del árbol ReusableTreeViewDialog.
 *
 * <p><img src="doc-files/ReusableTreeNode_class.png" alt="ReusableTreeNode"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura.
 */
public class ReusableTreeNode extends DefaultMutableTreeNode {
    /**
     * Elemento asociado al nodo.
     */
    private Container content = null;
    
    /**
     * Constructor por defecto.
     */
    ReusableTreeNode()
    {
        super();
    }

    /**
     * Constructor con objeto de usuario.
     * 
     * @param userObject Object objeto de usuario para el nodo.
     */
    ReusableTreeNode(Object userObject)
    {
        super(userObject);
    }

    /**
     * Establece el contenido del nodo.
     * 
     * @param content Container contenido del nodo.
     */
    public void setContent(Container content) {
        this.content = content;
    }

    /**
     * Devuelve el contenido del nodo.
     * 
     * @return Container contenido del nodo.
     */
    public Container getContent() {
        return this.content;
    }

    /**
     * Devuelve la representación del nodo en formato texto.
     * 
     * @return String representación del nodo en formato texto.
     */
    public String toString()
    {
        return userObject.toString();
    }    
}