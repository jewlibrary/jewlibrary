/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary;

import es.uned.reusablelibrary.exceptions.ReusableCleanException;
import es.uned.reusablelibrary.exceptions.ReusableSaveException;
import es.uned.reusablelibrary.exceptions.ReusableValidateException;
import es.uned.reusablelibrary.observer.EventManager;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * GUI reusable extensible, clase abstracta que es la base de la biblioteca y que implementa la funcionalidad común al resto de los GUI particulares.
 * 
 * La clase extiende de JPanel, para que sea compatible con el resto de contenedores de Swing, y se pueda utilizar como si fuese otro JPanel más.
 * 
 * <p><img src="doc-files/ReusableExtensibleDialog_class.png" alt="ReusableExtensibleDialog"></p>
 * 
 * @version 1.0
 *
 * @author Marcos Martínez Poladura
 */
public abstract class ReusableExtensibleDialog extends javax.swing.JPanel {
    
    /**
     * Gestor de eventos para la clase, que implementa el patrón Observer que gestiona el paso de mensajes entre las GUIs.
     */
    protected static EventManager events;
    
    /**
     * Nombre del GUI.
     */
    protected String contName = null;
    
    /**
     * Referencia al padre del diálogo (el GUI que lo contiene).
     */
    protected ReusableExtensibleDialog parentGUI = null;

    /**
     * Lista completa de componentes del GUI, se necesita para que al pulsar los botones de Aceptar o Cancelar, podamos recorrer la totalidad de los componentes y realizar sobre los mismos las operaciones de validación o cancelación individualmente.
     */
    protected ArrayList<ReusableFormInterface> components = new ArrayList();
        
    /**
     * Lista de hijos, que son GUI ReusableExtensibleDialog que están contenidos dentro del actual.
     */
    protected ArrayList<ReusableExtensibleDialog> children = new ArrayList<ReusableExtensibleDialog>();
    
    /**
     * Añade un GUI ReusableExtensibleDialog hijo al GUI actual. Establece el GUI actual como su padre.
     * 
     * @param extensibleChild ReusableExtensibleDialog GUI a añadir.
     * @param caption String etiqueta del contenedor.
     * @throws java.lang.Exception excepción si falla el añadir hijo.
     */
    public void addExtensibleChild(ReusableExtensibleDialog extensibleChild, String caption)
        throws Exception 
    {
        extensibleChild.setParentGUI(this);
        extensibleChild.setContainerName(caption);
        this.children.add(extensibleChild);
    }

    /**
     * Añade una lista de GUIs hijos, con un único nombre de contenedor para todos. Establece el GUI actual como su padre.
     * 
     * @param extensibleChildrenList ArrayList lista de GUIs hijos
     * @param caption String etiqueta del contenedor
     * @throws Exception se lanza en caso de error al añadir la lista de hijos
     */
    public void addExtensibleChildrenList(ArrayList<ReusableExtensibleDialog> extensibleChildrenList, String caption) 
        throws Exception 
    {
        for (int i = 0, max = extensibleChildrenList.size(); i < max; i++) {
            ReusableExtensibleDialog child = extensibleChildrenList.get(i);
            addExtensibleChild(child, caption);
        }
    }
        
    /**
     * Establece el nombre del GUI.
     * 
     * @param contName String nombre del GUI.
     */
    public void setContainerName(String contName) {
        this.contName = contName;
    }
   
    /**
     * Recupera el nombre del GUI.
     * 
     * @return String nombre del GUI.
     */
    public String getContainerName() {
        return this.contName;
    }

    @Override
    public Container getParent() {
        return super.getParent(); 
    }
    
    /**
     * Devuelve el padre del GUI.
     * 
     * @return ReusableExtensibleDialog padre del diálogo.
     */
    public ReusableExtensibleDialog getParentGUI() {
        return this.parentGUI;
    }
    
    /**
     * Devuelve el padre del diálogo.
     * 
     * @param parentGUI ReusableExtensibleDialog diálogo padre del actual.
     */
    public void setParentGUI(ReusableExtensibleDialog parentGUI) {
        this.parentGUI = parentGUI;
    }

    /**
     * Inicialización del GUI. Método abstracto que se implementará en las clases específicas.
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario.
     */
    public abstract Boolean setUpDialog();

    /**
     * Crea el JFrame para el GUI principal con los botones de aceptar y cancelar, con el tipo de redimensión FREE_DIALOG por defecto (el tipo de redimensión permite que se cambie el tamaño del GUI).
     * 
     * @param name String el título de la ventana que se mostrará en el escritorio.
     * 
     * @return JFrame el diálogo principal creado.
     */
    public JFrame exec(String name) {
        return this.exec(name, DialogResizer.ResizerType.FREE_DIALOG);
    }

    /**
     * Crea el JFrame para el GUI principal con los botones de aceptar y cancelar, permitiendo seleccionar el timpo de redimensión que va a tener (FREE_DIALOG o FIXED_DIALOG).
     * 
     * @param name String el título de la ventana que se mostrará en el escritorio.
     * @param resizerType ResizerType tipo de redimensión que tendrá la ventana (FREE_DIALOG o FIXED_DIALOG).
     * @return JFrame el diálogo principal creado.
     */
    public JFrame exec(String name, DialogResizer.ResizerType resizerType) {
        JFrame mainDialog = new JFrame(name);
        mainDialog.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        mainDialog.getContentPane().setLayout(new BorderLayout());
        
        JPanel jPanelBotones = new JPanel();
        jPanelBotones.setLayout(new FlowLayout());
        JButton jButtonOK = new javax.swing.JButton();
        JButton jButtonCancel = new javax.swing.JButton();
        
        jButtonOK.setText("OK");
        jButtonOK.setMinimumSize(new Dimension(20,30));
        jButtonOK.setMaximumSize(new Dimension(150,30));
        jButtonOK.setPreferredSize(new Dimension(120,30));

        ValidationListener validationListener = new ValidationListener(this);
        CancelListener cancelListener = new CancelListener(this);
        jButtonOK.addActionListener(validationListener);
        jPanelBotones.add(jButtonOK);

        jButtonCancel.setText("CANCELAR");
        jButtonCancel.setMinimumSize(new Dimension(20,30));
        jButtonCancel.setMaximumSize(new Dimension(150,30));
        jButtonCancel.setPreferredSize(new Dimension(120,30));
        jButtonCancel.addActionListener(cancelListener);
        jPanelBotones.add(jButtonCancel);

        mainDialog.getContentPane().add(jPanelBotones, BorderLayout.SOUTH);
        
        // AÑADIR LOS CONTENIDOS DE this.content y sus hijos
        mainDialog.getContentPane().add(this, BorderLayout.CENTER);
        mainDialog.pack();
        mainDialog.setResizable(true);
        DialogResizer.setupDialog(mainDialog, this);
        // mainDialog.setSize(DialogResizer.getWidth(), DialogResizer.getHeight());
        if (resizerType == DialogResizer.ResizerType.FIXED_DIALOG) {
            mainDialog.setResizable(false);
        }
        mainDialog.setVisible(true);
        
        return mainDialog;
    }    
    
    /**
     * Añadir contenidos al GUI actual.
     * 
     * @param content Container contenido que se añade al GUI.
     * @param caption String nombre del contenido añadido.
     * @throws Exception excepción lanzada en caso de error al añadir el contenido.
     */
    public abstract void append(Container content, String caption) throws Exception;
    
    /** 
     * Valida los campos del GUI apoyándose en la lista de componentes. En caso de que haya algún problema con la validación, mostrará un diálogo con el mensaje de error.
     * 
     * @return boolean TRUE si se han validado todos los componentes sin problemas, FALSE en caso contrario.
     */
    public boolean validateAll() {
        boolean allOK = true;
        int numComponents = this.components.size();
        int i = 0;
        while (i < numComponents && allOK) {
            ReusableFormInterface component = this.components.get(i);
            try {
                allOK = allOK && component.validateThis();
            }
            catch (ReusableValidateException e) {
                JOptionPane.showMessageDialog(this, e.getMessage());
                return false;
            }
            i++;
        }
        if (allOK) {
            int numChildren = this.children.size();
            int j = 0;
            while (j < numChildren && allOK) {
                ReusableExtensibleDialog child = this.children.get(j);
                allOK = allOK && child.validateAll();
                j++;
            }
        }
        return allOK;
    }

    /** 
     * Guarda los campos del GUI apoyándose en la lista de componentes. En caso de que haya algún problema al guardar, mostrará un diálogo con el mensaje de error.
     * 
     * @return boolean TRUE si han guardado todos los componentes sin problemas, FALSE en caso contrario.
     */
    public boolean saveAll() {
        boolean allOK = true;
        int numComponents = this.components.size();
        int i = 0;
        while (i < numComponents && allOK) {
            ReusableFormInterface component = this.components.get(i);
            try {
                component.saveThis();
            }
            catch (ReusableSaveException e) {
                allOK = false;
                JOptionPane.showMessageDialog(this, e.getMessage());
            }
            i++;
        }

        if (allOK) {
            int numChildren = this.children.size();
            int j = 0;
            while (j < numChildren && allOK) {
                ReusableExtensibleDialog child = this.children.get(j);
                allOK = allOK && child.saveAll();
                j++;
            }
        }
        return allOK;
    }

    /** 
     * Realiza tareas de finalización, cierre de ficheros, etc. para el GUI apoyándose en la lista de componentes. En caso de que haya algún problema con las tareas, mostrará un diálogo con el mensaje de error.
     * 
     * @return boolean TRUE si han limpiado todos los componentes sin problemas, FALSE en caso contrario.
     */
    public boolean cleanAll() {
        boolean allOK = true;
        int numComponents = this.components.size();
        int i = 0;
        while (i < numComponents && allOK) {
            ReusableFormInterface component = this.components.get(i);
            try {
                component.cleanThis();
            }
            catch (ReusableCleanException e) {
                JOptionPane.showMessageDialog(this, e.getMessage());
                return false;
            }
            i++;
        }
        if (allOK) {
            int numChildren = this.children.size();
            int j = 0;
            while (j < numChildren && allOK) {
                ReusableExtensibleDialog child = this.children.get(j);
                allOK = allOK && child.cleanAll();
                j++;
            }
        }
        return allOK;
    }

    /**
     * Devuelve el gestor de eventos que implementa el patrón Observer que gestiona el paso de mensajes entre las GUIs. Este método se llama desde las clases diseñadas para poder indicar qué componentes pasan mensajes a cuáles.
     * 
     * @return EventManager gestor de eventos que implementa el patrón Observer.
     */
    public static EventManager getEventManager() {
        return ReusableExtensibleDialog.events;
    }    
}
