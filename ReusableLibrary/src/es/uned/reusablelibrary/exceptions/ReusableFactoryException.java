/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary.exceptions;

/**
 * Excepción por errores producidos en la clase factoría.
 *
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ReusableFactoryException extends Exception {
    
    /**
     * Constructor por defecto.
     */
    public ReusableFactoryException() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param error String texto del error.
     */
    public ReusableFactoryException(String error) {
        super(error);
    }
}