/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.reusablelibrary;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JFrame;

/**
 * Listener para gestionar el cambio de tamaño del frame.
 * 
 * @version 1.0

* @author Marcos Martínez Poladura
 */
public class DialogResizerComponentListener implements ComponentListener {

    /**
     * el frame con el que se está trabajando.
     */
    private JFrame frame;
    
    /**
     * El diálogo principal del frame.
     */
    private ReusableExtensibleDialog mainDialog;
    
    /**
     * Ancho preferido.
     */
    private int preferredWidth = 0;
    
    /**
     * Alto preferido.
     */
    private int preferredHeight = 0;
    
    /**
     * Constructor
     * @param frame JFrame la ventana principal que se va a mostrar.
     * @param mainDialog ReusableExtensibleDialog el diálogo principal.
     */
    public DialogResizerComponentListener(JFrame frame, ReusableExtensibleDialog mainDialog) {
        this.frame = frame;
        this.mainDialog = mainDialog;
        this.preferredWidth = this.frame.getWidth();
        this.preferredHeight = this.frame.getHeight();
        this.frame.getContentPane().setPreferredSize(new Dimension(this.preferredWidth, this.preferredHeight));
        this.frame.pack();
    }
    
    @Override
    public void componentResized(ComponentEvent e) {
        DialogResizer.updateSize(this.frame, this.mainDialog);
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }    
}