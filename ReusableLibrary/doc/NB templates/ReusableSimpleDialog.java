/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
<#if package?? &amp;&amp; package != "">
package ${package};

</#if>

import es.uned.reusablelibrary.exceptions.ReusableSimpleDialogException;
import es.uned.reusablelibrary.observer.EventManager;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.io.File;
import java.io.PrintStream;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * Implementación del GUI de una sola página (simple).
 * 
 * <p><img src="doc-files/ReusableSimpleDialog_class.png" alt="ReusableSimpleDialog"></p>
 * 
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ${name} extends ReusableExtensibleDialog {
    /**
     * Nº máximo de GUIs simples que se pueden integrar en una página.
     */
    private static final int MAX_GUI_SIMPLE = 2;

    /**
     * Número de elementos añadidos.
     */
    private int numElements = 0;

    /**
     * Tipo de distribución (layout) del GUI, por defecto BOX_LAYOUT_VERTICAL.
     */
    private ReusableDialogFactory.LayoutType layoutType = ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL;

    /**
     * Constructor por defecto (imprescindible para poder meter el componente en la paleta).
     */
    public ${name}() {
        super.contName="Simple";
        // Establece el parentGUI
        Component c = this;
        while (c != null) {
            c = c.getParent();
            if (c != null && c.getClass().isAssignableFrom(ReusableExtensibleDialog.class)) {
                super.setParentGUI((ReusableExtensibleDialog) c);
                break;
            }
        }
        initComponents();
    }

    /**
     * Constructor.
     * 
     * @param parent ReusableExtensibleDialog contenedor padre del elemento.
     * @param layoutType tipo de layout.
     * @param caption el nombre del contenedor.
     */
    public ${name}(ReusableExtensibleDialog parent, ReusableDialogFactory.LayoutType layoutType, String caption) {
        initComponents();
        super.contName = caption;
        this.layoutType = layoutType;
    }

    /**
     * Inicialización del GUI. Método abstracto que se implementará en las clases específicas.
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario.
     */
    @Override
    public Boolean setUpDialog() {
        if (ReusableExtensibleDialog.events == null) {
            ReusableExtensibleDialog.events = new EventManager();
        }
        
        // Crear el panel con un SplitPane con 2 componentes.
        JPanel simple = new JPanel();
            
        if (null == this.layoutType) return false;
        
        else switch (this.layoutType) {
            case BOX_LAYOUT_VERTICAL:
                simple.setLayout(new BoxLayout(simple, BoxLayout.Y_AXIS));
                break;
            case BOX_LAYOUT_HORIZONTAL:
                simple.setLayout(new BoxLayout(simple, BoxLayout.X_AXIS));
                break;
            default:
                return false;
        }
            
        super.add(simple);
        return true;
    }
    
    /**
     * Añade un GUI ReusableExtensibleDialog hijo al GUI actual. Establece el GUI actual como su padre.
     * 
     * @param extensibleChild ReusableExtensibleDialog GUI a añadir.
     * @param caption String etiqueta del contenedor.
     * @throws java.lang.Exception excepción si falla el añadir hijo.
     */
    @Override
    public void addExtensibleChild(ReusableExtensibleDialog extensibleChild, String caption) 
        throws Exception 
    {
        super.addExtensibleChild(extensibleChild, caption);
        ((JPanel)super.getComponent(0)).add(extensibleChild.getComponent(0), caption);
    }
    
    /**
     * Añadir contenidos al GUI actual.
     * 
     * @param content Container contenido que se añade al GUI.
     * @param caption String nombre del contenido añadido.
     * @throws Exception excepción lanzada en caso de error al añadir el contenido.
     */
    @Override
    public void append(Container content, String caption)
        throws Exception 
    {
        if (this.numElements < ${name}.MAX_GUI_SIMPLE) {
            this.numElements++;

            if (content.getClass().isAssignableFrom(ReusableExtensibleDialog.class)) {
                this.addExtensibleChild((ReusableExtensibleDialog)content, caption);
            }
            else {
                ((JPanel)super.getComponent(0)).add(content);
                super.components.add((ReusableFormInterface)content);
            }
        }
        else { 
            throw new ReusableSimpleDialogException("Error al añadir hijo. El nº máximo de elementos es 2");
        }
    }

    /**
     * Establece el tipo de layout.
     * 
     * @param layoutType LayoutType tipo de layout.
     */
    public void setLayoutType(ReusableDialogFactory.LayoutType layoutType) {
        if (layoutType == ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL) {
            this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        }
        if (layoutType == ReusableDialogFactory.LayoutType.BOX_LAYOUT_HORIZONTAL) {
            this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        }
        this.layoutType = layoutType;
    }

    
    @Override
    public void setLayout(LayoutManager mgr) {
        if (mgr.getClass().isAssignableFrom(BoxLayout.class)) {
            if (((BoxLayout)mgr).getAxis() == BoxLayout.Y_AXIS) {
                this.layoutType = ReusableDialogFactory.LayoutType.BOX_LAYOUT_VERTICAL;
                super.setLayout(mgr);
            }
            else if (((BoxLayout)mgr).getAxis() == BoxLayout.X_AXIS) {
                this.layoutType = ReusableDialogFactory.LayoutType.BOX_LAYOUT_HORIZONTAL;
                super.setLayout(mgr);
            }
        }
        // En otro caso, se deja el layout que estaba, no se pone uno nuevo.
    }
    
    /**
     * Recupera el tipo de layout.
     * 
     * @return LayoutType tipo de layout.
     */
    public ReusableDialogFactory.LayoutType getLayoutType() {
        return this.layoutType;
    }

    @Override
    public Container getParent() {
        return super.getParent(); 
    }
    
    @Override
    public String toString() {
        String result = "";
        result += "ReusableSimpleDialog\n";
        result += "--------------------\n";
        result += "super.content: " + super.getClass().toString() + " - " + super.getComponentCount() + " componentes \n";
        for (int i = 0, max = super.getComponents().length; i < max; i++) {
            Component component = super.getComponents()[i];
            if (component.getClass().toString().indexOf("Dialog") != -1) {
                result += "componente (" + component.getClass().toString() + "): " + component + "\n";
            }
            else {
                result += "componente (" + component.getClass().toString() + ")\n";
            }
        }
        return result;
    }        
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.Y_AXIS));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    /**
     * Método para llamar initComponents desde las clases hijas.
     */
    protected void init() {
        initComponents();
    }
}
