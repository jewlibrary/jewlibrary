/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
<#if package?? &amp;&amp; package != "">
package ${package};

</#if>


import es.uned.reusablelibrary.observer.EventManager;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;

/**
 * Implementación del GUI con navegación mediante vista de árbol.
 * 
 * <p><img src="doc-files/ReusableTreeViewDialog_class.png" alt="ReusableTreeViewDialog"></p>
 *
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ${name} extends ReusableExtensibleDialog {
    /**
     * Nombre del árbol.
     */
    private String navigatName;

    /**
     * Ancho máximo del panel de contenido.
     */
    private double maxContentW = 0.0;

    /**
     * Alto máximo del panel de contenido.
     */
    private double maxContentH = 0.0;

    /**
     * Constructor por defecto (imprescindible para poder meter el componente en la paleta).
     */
    public ${name}() {
        this.navigatName = "Árbol";
        this.contName = "Árbol";
        // Establece el parentGUI
        Component c = this;
        while (c != null) {
            c = c.getParent();
            if (c != null && c.getClass().isAssignableFrom(ReusableExtensibleDialog.class)) {
                super.setParentGUI((ReusableExtensibleDialog) c);
                break;
            }
        }
        initComponents();
        navigatorName.setText(this.navigatName);
    }

    /**
     * Constructor.
     * 
     * @param parent ReusableExtensibleDialog padre del diálogo.
     * @param caption String el nombre del contenedor.
     */
    public ${name}(ReusableExtensibleDialog parent, String caption) {
        this.contName = caption;
        this.navigatName = "";
        initComponents();
        navigatorName.setVisible(false);
    }

    /**
     * Constructor.
     * 
     * @param parent ReusableExtensibleDialog padre del diálogo.
     * @param caption String el nombre del contenedor.
     * @param navigatName String la etiqueta de encima del árbol.
     */
    public ${name}(ReusableExtensibleDialog parent, String caption, String navigatName) {
        this.contName = caption;
        this.navigatName = navigatName;
        initComponents();
        navigatorName.setText(this.navigatName);
    }

    /**
     * Inicialización del GUI. Método abstracto que se implementará en las clases específicas.
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario.
     */
    @Override
    public Boolean setUpDialog() {
        if (ReusableExtensibleDialog.events == null)
            ReusableExtensibleDialog.events = new EventManager();
        
        try {
            // Crear página con el componente árbol a la izquierda, y espacio a la derecha para mostrar los resultados.
            this.jTree.addComponentListener(new ReusableTreeViewComponentListener(this.jTree));
            navigatorName.setText(this.getTreeNavigatorName());
            expandAllNodes(this.jTree, 0, this.jTree.getRowCount());
            jSplitPane1.getBottomComponent().setMinimumSize(new Dimension((int)this.maxContentW, (int)this.maxContentH));
            super.add(jSplitPane1);
            
            this.jTree.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) 
                {
                    if (e.getClickCount() == 1) {
                        ReusableTreeNode node = (ReusableTreeNode)
                               jTree.getLastSelectedPathComponent();
                        
                        if (node == null) {
                            jSplitPane1.setBottomComponent(new JPanel());
                            return;
                        }
                        if (node.getContent() == null) { 
                            jSplitPane1.setBottomComponent(new JPanel());
                            return;
                        }
                        
                        // Rellenar el panel derecho con el contenido.
                        JPanel panel = (JPanel) jSplitPane1.getBottomComponent();
                        jSplitPane1.setBottomComponent(node.getContent());
                    }
                }
            });
            return true;
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
            return false;
        }
    }

    /**
     * Establecer el nombre del árbol.
     * 
     * @param navigatName String nombre del árbol.
     */
    public void setTreeNavigatorName(String navigatName) {
        this.navigatName = navigatName;
    }

    /**
     * Recuperar el nombre del árbol.
     * 
     * @return String nombre del árbol.
     */
    public String getTreeNavigatorName() {
        return this.navigatName;
    }

    /**
     * Añade un GUI ReusableExtensibleDialog hijo al GUI actual. Establece el GUI actual como su padre.
     * 
     * @param extensibleChild ReusableExtensibleDialog GUI a añadir.
     * @param caption String etiqueta del contenedor.
     * @throws java.lang.Exception excepción si falla el añadir hijo.
     */
    @Override
    public void addExtensibleChild(ReusableExtensibleDialog extensibleChild, String caption) 
        throws Exception
    {
        super.addExtensibleChild(extensibleChild, caption);

        ReusableTreeNode node = new ReusableTreeNode(caption);
        node.setUserObject(caption);
        node.setContent(extensibleChild);
        ReusableTreeNode root =
            (ReusableTreeNode)this.jTree.getModel().getRoot();
        root.insert(node, root.getChildCount());
    }

    /**
     * Añade un GUI hijo al nodo indicado del árbol.
     * 
     * @param extensibleChild ReusableExtensibleDialog GUI a añadir.
     * @param node ReusableTreeNode nodo actual en el cual se inserta el nuevo nodo.
     * @param caption String etiqueta del contenedor.
     * @throws Exception excepción en caso de error al añadir el GUI hijo.
     */
    public void addExtensibleChild(ReusableExtensibleDialog extensibleChild, ReusableTreeNode node, String caption) 
        throws Exception
    {
        super.addExtensibleChild(extensibleChild, caption);

        ReusableTreeNode newNode = new ReusableTreeNode(caption);
        node.setUserObject(caption);
        newNode.setContent(extensibleChild);
        node.insert(newNode, node.getChildCount());
    }

    /**
     * Añadir contenido adicional al nodo del árbol indicado.
     * 
     * @param content Container elemento a incluir en el diálogo. Si es null, solo incluye la etiqueta.
     * @param node ReusableTreeNode nodo actual en el cual se inserta el nuevo nodo.
     * @param caption String Etiqueta del nodo en el árbol.
     * @return ReusableTreeNode el nodo que acabamos de insertar.
     */
    public ReusableTreeNode append(Container content, ReusableTreeNode node, String caption)
    {
        ReusableTreeNode newNode = new ReusableTreeNode(caption);
        if (content != null) {
            newNode.setContent(content);
            super.components.add((ReusableFormInterface)content);
            if (content.getMinimumSize().getWidth() > this.maxContentW)
                this.maxContentW = content.getMinimumSize().getWidth();
            if (content.getMinimumSize().getHeight() > this.maxContentH)
                this.maxContentH = content.getMinimumSize().getHeight();
        }
        if (node != null) {
            node.insert(newNode, node.getChildCount());
        }
        else {
            ReusableTreeNode root =
                (ReusableTreeNode)this.jTree.getModel().getRoot();
            root.insert(newNode, root.getChildCount());
        }
        return newNode;
    }

    /**
     * Devuelve la anchura del árbol, que está en el panel izquierdo del diálogo.
     * 
     * @return int Ancho del árbol.
     */
    public int getTreeWidth() {
        return (int) ((JSplitPane)super.getComponent(0)).getTopComponent().getPreferredSize().getWidth();
    }
    
    /**
     * Añadir contenidos al GUI actual.
     * 
     * @param content Container contenido que se añade al GUI.
     * @param caption String nombre del contenido añadido.
     */
    @Override
    public void append(Container content, String caption)
    {            
        ReusableTreeNode newNode = new ReusableTreeNode(caption);
        if (content != null) {
           newNode.setContent(content);
            super.components.add((ReusableFormInterface)content);
            if (content.getMinimumSize().getWidth() > this.maxContentW)
                this.maxContentW = content.getMinimumSize().getWidth();
            if (content.getMinimumSize().getHeight() > this.maxContentH)
                this.maxContentH = content.getMinimumSize().getHeight();
        }
        
        ReusableTreeNode root =
            (ReusableTreeNode)this.jTree.getModel().getRoot();
        root.insert(newNode, root.getChildCount());
    }

    @Override
    public Container getParent() {
        return super.getParent(); 
    }

    /**
     * Expande todos los nodos del arbol. Uso: expandAllNodes (tree, 0, tree.getRowCount());
     * 
     * @param tree JTree el árbol que queremos expandir.
     * @param startingIndex int el nodo inicial.
     * @param rowCount int número de nodos.
     */
    private void expandAllNodes(JTree tree, int startingIndex, int rowCount){
        for(int i=startingIndex;i<rowCount;++i){
            tree.expandRow(i);
        }

        if(tree.getRowCount()!=rowCount){
            expandAllNodes(tree, rowCount, tree.getRowCount());
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        jSplitPane2 = new javax.swing.JSplitPane();
        navigatorName = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTree = new javax.swing.JTree();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 214, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 298, Short.MAX_VALUE)
        );

        jSplitPane1.setRightComponent(jPanel1);

        jSplitPane2.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        navigatorName.setText("Nombre Árbol");
        navigatorName.setName("navigatorName"); // NOI18N
        navigatorName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navigatorNameActionPerformed(evt);
            }
        });
        jSplitPane2.setTopComponent(navigatorName);

        jTree.setModel(new DefaultTreeModel((new ReusableTreeNode(this.contName))));
        jScrollPane2.setViewportView(jTree);

        jSplitPane2.setRightComponent(jScrollPane2);

        jSplitPane1.setLeftComponent(jSplitPane2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void navigatorNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_navigatorNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_navigatorNameActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JPanel jPanel1;
    protected javax.swing.JScrollPane jScrollPane2;
    protected javax.swing.JSplitPane jSplitPane1;
    protected javax.swing.JSplitPane jSplitPane2;
    protected javax.swing.JTree jTree;
    protected javax.swing.JButton navigatorName;
    // End of variables declaration//GEN-END:variables

    /**
     * Método para llamar initComponents desde las clases hijas.
     */
    protected void init() {
        initComponents();
    }
}