/*
 * Copyright (C) 2021 Marcos Martínez Poladura.
 *
 * This file is part of ReusableLibrary.

 * ReusableLibrary is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
<#if package?? &amp;&amp; package != "">
package ${package};

</#if>


import es.uned.reusablelibrary.observer.EventManager;
import java.awt.Component;
import java.awt.Container;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 *
 * Implementación del GUI de pestañas.
 *
 * <p><img src="doc-files/ReusableTabDialog_class.png" alt="ReusableTabDialog"></p>
 *
 * @version 1.0
 * 
 * @author Marcos Martínez Poladura
 */
public class ${name} extends ReusableExtensibleDialog {

    /**
     * Constructor por defecto (imprescindible para poder meter el componente en la paleta).
     */
    public ${name}() {
        this.contName = "Tab";
        initComponents();
    }

    /**
     * Constructor.
     * 
     * @param parent ReusableExtensibleDialog contenedor padre del elemento.
     * @param caption String el nombre del contenedor.
     */
    public ${name}(ReusableExtensibleDialog parent, String caption) {
        this.setParentGUI(parent);
        this.contName = caption;
        initComponents();
        tabPanel.remove(0);
    }

    /**
     * Inicialización del GUI. Método abstracto que se implementará en las clases específicas.
     * 
     * @return Boolean true si ha inicializado con éxito, false en caso contrario.
     */
    @Override
    public Boolean setUpDialog() {
        if (ReusableExtensibleDialog.events == null)
            ReusableExtensibleDialog.events = new EventManager();
        
        return true;
    }  
    
    /**
     * Añade un GUI ReusableExtensibleDialog hijo al GUI actual. Establece el GUI actual como su padre.
     * 
     * @param extensibleChild ReusableExtensibleDialog GUI a añadir.
     * @param caption String etiqueta del contenedor.
     * @throws java.lang.Exception excepción si falla el añadir hijo.
     */
    @Override
    public void addExtensibleChild(ReusableExtensibleDialog extensibleChild, String caption) 
        throws Exception
    {
        super.addExtensibleChild(extensibleChild, caption);
        ((JTabbedPane)((JPanel)super.getComponent(0)).getComponent(0)).add(extensibleChild.getComponent(0), caption);
    }

    /**
     * Añadir contenidos al GUI actual.
     * 
     * @param content Container contenido que se añade al GUI.
     * @param caption String nombre del contenido añadido.
     */
    @Override
    public void append(Container content, String caption)
    {            
        // Crear un nuevo TAB
        if (content.getClass().isAssignableFrom(ReusableExtensibleDialog.class)) {
            try {
                this.addExtensibleChild((ReusableExtensibleDialog)content, caption);
            }
            catch(Exception e) {
                JOptionPane.showMessageDialog(this, e.getMessage());
            }
        }
        System.out.println("append.super.getComponent(0) = " + super.getComponent(0).getClass().getCanonicalName());
        
        ((JTabbedPane)((JPanel)super.getComponent(0)).getComponent(0)).addTab(caption, content);
        super.components.add((ReusableFormInterface)content);
    }

    @Override
    public String toString() {
        String result = "";
        result += "ReusableTabDialog\n";
        result += "-----------------\n";
        result += "super.content: " + ((JTabbedPane)((JPanel)super.getComponent(0)).getComponent(0)).getClass().toString() + " - " + ((JTabbedPane)((JPanel)super.getComponent(0)).getComponent(0)).getComponentCount() + " componentes \n";
        for (int i = 0, max = ((JTabbedPane)((JPanel)super.getComponent(0)).getComponent(0)).getComponents().length; i < max; i++) {
            Component component = ((JTabbedPane)((JPanel)super.getComponent(0)).getComponent(0)).getComponents()[i];
            result += "componente " + component.getName() + " (" + component.getClass().toString() + ")\n";
        }
        return result;
    }

    @Override
    public Container getParent() {
        return super.getParent(); 
    }

    @Override
    public Component add(Component comp) {
       return ((JTabbedPane)((JPanel)super.getComponent(0)).getComponent(0)).add(comp);
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedPaneContainer = new javax.swing.JPanel();
        tabPanel = new javax.swing.JTabbedPane();
        tab1 = new javax.swing.JPanel();

        setLayout(new java.awt.BorderLayout());

        tabbedPaneContainer.setLayout(new java.awt.BorderLayout());

        tab1.setLayout(new java.awt.BorderLayout());
        tabPanel.addTab("tab1", tab1);

        tabbedPaneContainer.add(tabPanel, java.awt.BorderLayout.CENTER);

        add(tabbedPaneContainer, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JPanel tab1;
    protected javax.swing.JTabbedPane tabPanel;
    private javax.swing.JPanel tabbedPaneContainer;
    // End of variables declaration//GEN-END:variables

    /**
     * Método para llamar initComponents desde las clases hijas.
     */
    protected void init() {
        initComponents();
    }

}
